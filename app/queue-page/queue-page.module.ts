import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QueuePagePageRoutingModule } from './queue-page-routing.module';

import { QueuePagePage } from './queue-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QueuePagePageRoutingModule
  ],
  declarations: [QueuePagePage]
})
export class QueuePagePageModule {}
