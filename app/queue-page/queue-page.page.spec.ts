import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QueuePagePage } from './queue-page.page';

describe('QueuePagePage', () => {
  let component: QueuePagePage;
  let fixture: ComponentFixture<QueuePagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueuePagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QueuePagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
