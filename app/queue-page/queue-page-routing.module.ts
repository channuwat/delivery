import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QueuePagePage } from './queue-page.page';

const routes: Routes = [
  {
    path: '',
    component: QueuePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QueuePagePageRoutingModule {}
