import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { WabapiService } from '../wabapi.service';


@Component({
  selector: 'app-review',
  templateUrl: './review.page.html',
  styleUrls: ['./review.page.scss'],
})
export class ReviewPage implements OnInit {
  public res = '';
  public type = '';
  public tenantIDFileName = '';
  public showclickimg = false;
  imageURI:any;
  imageFileName:any;
  public rating = 0;
  fileData: File = null;
  data = {p_title:'', p_content:'',p_rating:0,id_user:1,id_res:0};
  previewUrl:any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;



  constructor(private route: ActivatedRoute, public navCtrl: NavController,private router: Router, private http: HttpClient,
     private transfer: FileTransfer,public service: WabapiService) { 
    this.res = this.route.snapshot.paramMap.get('res');
    this.type = this.route.snapshot.paramMap.get('type');

    service.getData('main/get_res/' + this.res + '?lang=th').then((result: any) => {
      console.log(result);
      if (result.flag) {
        this.data.id_res=result.data.id_res_auto;
      }
      });
  }

  ngOnInit() {
  }

  back() {
    // this.router.navigateByUrl('/' + this.res);  
    this.navCtrl.navigateRoot('/' + this.res);
  }

  openFileBrowser (event: any){
    event.preventDefault();
    let element: HTMLElement = document.getElementById('uploadfile') as HTMLElement;
    element.click();
  }
  onFileChange (event: any){
    let files = event.target.files;
    this.tenantIDFileName = files[0].name;

    console.log(files)
  }
  
  set_rating(r){
    this.rating = r;
  }

  fileProgress(fileInput: any) {
      this.fileData = <File>fileInput.target.files[0];
      this.preview();

  }
  preview() {
    // Show preview 
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
 
    var reader = new FileReader();      
    reader.readAsDataURL(this.fileData); 
    reader.onload = (_event) => { 
      this.previewUrl = reader.result; 
      this.showclickimg =true;
    }
  }
   
  onSubmit() {
    const fdata = new FormData();
    this.data.p_rating = this.rating;
    fdata.append('data',JSON.stringify(this.data));
    console.log(this.data);
    fdata.append('image', this.fileData, this.fileData.name);
    this.http.post('https://api.deltafood.co/reviews/upload', fdata,{
      reportProgress: true,
      observe: 'events'   
    })
    .subscribe(events => {
      if(events.type === HttpEventType.UploadProgress) {
        console.log('Upload Progress: ' + Math.round(events.loaded / events.total) * 100 + '%');
      } else if(events.type === HttpEventType.Response) {
        console.log(events);
        alert('SUCCESS !!');
      }
    })
}

}


