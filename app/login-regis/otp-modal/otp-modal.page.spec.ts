import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OtpModalPage } from './otp-modal.page';

describe('OtpModalPage', () => {
  let component: OtpModalPage;
  let fixture: ComponentFixture<OtpModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtpModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OtpModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
