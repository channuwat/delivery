import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OtpModalPageRoutingModule } from './otp-modal-routing.module';

import { OtpModalPage } from './otp-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    OtpModalPageRoutingModule
  ],
  declarations: [OtpModalPage]
})
export class OtpModalPageModule {}
