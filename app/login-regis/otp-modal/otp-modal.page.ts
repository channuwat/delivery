import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Storage } from '@ionic/storage';
import { WabapiService } from 'src/app/wabapi.service';
import { ModalController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-otp-modal',
  templateUrl: './otp-modal.page.html',
  styleUrls: ['./otp-modal.page.scss'],
})
export class OtpModalPage implements OnInit {

  @Input() data: any;
  public state_btn = ''
  public otpForm
  constructor(
    public modalCtrl: ModalController,
    public service: WabapiService,
    public store: Storage,
    private ref: ChangeDetectorRef
  ) {
    this.otpForm = new FormGroup(
      {
        'otp_serial': new FormControl('', Validators.compose([
          Validators.required,
          Validators.maxLength(6),
          Validators.minLength(6),
          Validators.pattern("([0-9]{6})")
        ]))
      });

    this.otpForm.setValue({
      otp_serial: ''
    });
  }

  ngOnInit() {

  }

  ionViewDidEnter() {
    if (this.data.state_btn == 'contact_call_center') {
      this.state_btn = 'contact'
      this.ref.detectChanges()
    } else {
      this.state_btn = 'add'
      this.ref.detectChanges()
    }

  }

  setButton() {

    if (this.data.state_btn == 'contact_call_center') {
      this.state_btn = 'contact'
      this.ref.detectChanges()
    } else {
      this.state_btn = 'add'
      this.ref.detectChanges()
    }
  }

  getOTP(re_otp) {
    this.service.getData('API_user/checkTimeOutpOTP/' + re_otp.phone).then((case_state: any) => {
      re_otp.state_btn = case_state
      if (re_otp.state_btn == 'wait_timeout') {
        this.service.Toast('โปรดรอ 3 นาทีเพื่อขอ OTP อีกรอบ !')
      } else if (re_otp.state_btn == 'updated' || re_otp.state_btn == 'inserted') {
        let phone = re_otp.phone
        this.data = {
          name: re_otp.name,
          phone: re_otp.phone,
          pass: re_otp.pass,
          state_btn: '',
          code: ''
        }

        if (phone != '') {
          this.service.postData('API_user/getOTP', { phone: phone }).then((data_e: any) => {
            this.data['state_btn'] = data_e.txt
            this.data['code'] = data_e.code
            this.setButton()
            console.log(data_e);
          })
        }

        this.service.Toast('ส่งคำร้องขอรหัส OTP เเล้ว √')
      } else {
        this.setButton()
      }
    })
  }

  verificationUser() {
    this.data['otp'] = this.otpForm.value.otp_serial
    this.service.postData('API_user/registerUser',this.data).then((respone:any)=>{
      if(respone.txt_error == 'บันทึกสำเร็จ'){
        this.service.Toast('ยินดีต้อนรับ')
        this.store.set('user', respone.data_respone)
        setTimeout(() => {
          this.close(null,'เข้าสู่ระบบสำเร็จ')
        }, 800);
      }else{
        this.service.Toast('โปรดระบุ OTP ให้ถูกต้อง')
        this.otpForm.patchValue({ otp_serial: '' })
      }
      
    })

  }

  close(data, txt_close) {
    this.modalCtrl.dismiss(data, txt_close)
  }

}
