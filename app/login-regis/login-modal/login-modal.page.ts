import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { WabapiService } from 'src/app/wabapi.service';
import { Storage } from '@ionic/storage';
import { RegisModalPage } from '../regis-modal/regis-modal.page';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.page.html',
  styleUrls: ['./login-modal.page.scss'],
})
export class LoginModalPage implements OnInit {

  public loginForm
  constructor(
    public modalCtrl: ModalController,
    public service: WabapiService,
    public store: Storage
  ) {
    this.loginForm = new FormGroup({
      'phone_id': new FormControl('', Validators.compose([
        Validators.maxLength(10),
        Validators.minLength(10),
        Validators.pattern("0([0-9]{9})")
      ])),
      'phone_pass': new FormControl('', Validators.compose([]))
    });

    this.loginForm.setValue({
      phone_id: '',
      phone_pass: '',
    });
  }

  ngOnInit() {
  }

  loginUser() {
    let login_data = {
      id: this.loginForm.value.phone_id,
      pass: this.loginForm.value.phone_pass,
    }
    this.service.postData('API_user/loginUser', login_data).then((response_data: any) => {
      if (response_data.message == 'สำเร็จ') {
        this.service.Toast('ยินดีต้อนรับ')
        this.store.set('user', response_data)
        setTimeout(() => {
          this.close(null, 'เข้าสู่ระบบสำเร็จ')
        }, 800);

      } else {
        this.service.Toast('โปรดกรอกเบอร์โทรศัพท์หรือรหัสผ่านให้ถูกต้อง')
        this.loginForm.patchValue({ phone_pass: '' })
      }
    })
  }

  async opemModal() {

    const modal = await this.modalCtrl.create({
      component: RegisModalPage,
      componentProps: { data: {} },
      backdropDismiss: false,
      cssClass: 'regis_modal',
    });

    modal.onWillDismiss().then((event: any) => {
      
    })

    return await modal.present();
  }

  close(data, txt_close) {
    this.modalCtrl.dismiss(data, txt_close)
  }

}
