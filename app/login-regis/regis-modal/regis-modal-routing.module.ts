import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisModalPage } from './regis-modal.page';

const routes: Routes = [
  {
    path: '',
    component: RegisModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisModalPageRoutingModule {}
