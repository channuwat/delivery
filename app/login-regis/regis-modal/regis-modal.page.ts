import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { WabapiService } from 'src/app/wabapi.service';
import { Storage } from '@ionic/storage';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-regis-modal',
  templateUrl: './regis-modal.page.html',
  styleUrls: ['./regis-modal.page.scss'],
})
export class RegisModalPage implements OnInit {

  public regisForm
  constructor(
    public modalCtrl: ModalController,
    public service: WabapiService,
    public store: Storage,
    private ref: ChangeDetectorRef
  ) {
    this.regisForm = new FormGroup(
      {
        'name_reg': new FormControl('', Validators.compose([])),
        'phone_id_reg': new FormControl('', Validators.compose([
          Validators.required,
          Validators.maxLength(10),
          Validators.minLength(10),
          Validators.pattern("0([0-9]{9})")
        ])),
        'phone_pass_reg': new FormControl('', Validators.compose([
          Validators.required,
          Validators.maxLength(13),
          Validators.minLength(6),
        ]))
      });

    this.regisForm.setValue({
      name_reg: '',
      phone_id_reg: '',
      phone_pass_reg: '',
    });

  }

  ngOnInit() {
  }

  public data_reg = {
    name: '',
    phone: '',
    pass: '',
  }
  public btn_event = { st_1: '', st_2: '' }
  closeValidate(verification) {
    // verification เอามากันลบ disabled ใน code f12
    let txt = ''
    if (!verification) {
      this.data_reg = {
        name: this.regisForm.value.name_reg,
        phone: this.regisForm.value.phone_id_reg,
        pass: this.regisForm.value.phone_pass_reg,
      }
      this.service.getData('API_user/checkRegisterUser/' + this.data_reg.phone).then((response: any) => {

        this.btn_event.st_1 = response.text
        this.btn_event.st_2 = response.event.txt
        this.ref.detectChanges()


        if (response.text == 'can_register') {
          if (response.event.txt == 'inserted' || response.event.txt == 'updated') {
            txt = 'ขอotp'
            this.getOTP()

            this.close(this.data_reg, txt)
          } else if (response.event.txt == 'wait_timeout') {
            this.service.Toast('โปรดรอ 3 นาทีเพื่อขอ OTP อีกรอบ!')
          }
        } else if (response == 'has_number_phone') {
          this.service.Toast('เบอร์โทรศัพท์นี้มีในระบบเเล้ว!')
        } else if (response == 'no_input') {
          this.service.Toast('โปรดระบุเบอร์โทรศพท์ให้ถูกต้อง!')
        }
      })

    } else {
      this.service.Toast('ผิดผลาดในการสมัคร!')
      txt = 'ผิดผลาดในการสมัคร'
      setTimeout(() => {
        this.close(null, txt)
      }, 1500);
    }
  }

  getOTP() {
    let phone = this.regisForm.value.phone_id_reg
    if (phone != '') {
      this.service.postData('API_user/getOTP', { phone: phone }).then((data_e: any) => {
        this.data_reg['state_btn'] = data_e.txt
        this.data_reg['code'] = data_e.code
      })
    }
  }

  close(data, txt_close) {
    this.modalCtrl.dismiss(data, txt_close)
  }

}
