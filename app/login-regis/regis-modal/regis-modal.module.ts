import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisModalPageRoutingModule } from './regis-modal-routing.module';

import { RegisModalPage } from './regis-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RegisModalPageRoutingModule
  ],
  declarations: [RegisModalPage]
})
export class RegisModalPageModule {}
