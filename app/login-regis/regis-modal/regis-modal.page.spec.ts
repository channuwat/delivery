import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegisModalPage } from './regis-modal.page';

describe('RegisModalPage', () => {
  let component: RegisModalPage;
  let fixture: ComponentFixture<RegisModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegisModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
