import { Component, OnInit, ChangeDetectorRef, Input, ViewChild, ElementRef } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { WabapiService } from 'src/app/wabapi.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { GoogleMapsEvent, LatLng } from '@ionic-native/google-maps/ngx';
declare var google;


@Component({
   selector: 'app-order-detail-page',
   templateUrl: './order-detail-page.page.html',
   styleUrls: ['./order-detail-page.page.scss'],
})
export class OrderDetailPagePage implements OnInit {

   public order_key = null
   @Input() order_id: any
   constructor(
      public modalCtrl: ModalController,
      public store: Storage,
      public changeDetector: ChangeDetectorRef,
      public alertController: AlertController,
      public navParams: NavParams,
      public api: WabapiService,
      private geolocation: Geolocation,
   ) {
      this.order_key = this.navParams.get('order_id')
   }


   ngOnInit() {
   }


   ionViewWillEnter() {

      this.setConfirmOrder(this.order_key)
   }


   @ViewChild('mapElement', { static: false }) mapNativeElement: ElementRef;
   public latitude: any;
   public longitude: any;
   loadMap(lat, long) {
      this.geolocation.getCurrentPosition().then((resp) => {
         const pos = {
            lat: parseFloat(lat),
            lng: parseFloat(long)
         };
         const map = new google.maps.Map(this.mapNativeElement.nativeElement, {
            center: pos,
            zoom: 17
         });
         /*location object*/

         map.setCenter(pos);
         const icon = {
            url: 'assets/icon/icon_map/u.png', // image url
            scaledSize: new google.maps.Size(50, 50), // scaled size
         };
         const marker = new google.maps.Marker({
            position: pos,
            map: map,
            title: 'Hello World!',
            icon: icon
         });

      }).catch((error) => {
         console.log('Error getting location', error);
      });
   }

   public data_details = {
      order:
      {
         address: "",
         member_lat_long: { lat: 0, long: 0 },
         member_comment: "",
         member_giveaway: "0",
      },
      slip: [{img_url:''}]
   }
   public total = 0
   setConfirmOrder(id_order) {
      this.api.getData('Delivery_order/getDetailOrder/' + id_order).then((detail: any) => {
         this.data_details = detail
         if(this.data_details.slip.length == 0){
            this.data_details.slip = [{img_url:''}]
         }
         let lat = this.data_details['order'].member_lat_long.lat
         let long = this.data_details['order'].member_lat_long.long
         this.loadMap(lat, long)
         this.store.get('store_foods').then(data_food => {
            data_food.forEach(item_food => {
               this.data_details['detail'].forEach(item_detail => {
                  if (item_food.f_id == item_detail.f_id) {
                     item_detail['name'] = item_food.name
                     this.total += parseInt(item_detail.total_price)
                  }
               });
            });
         })
      })
   }

   get_html(c): String {
      let out: String = '';
      for (let entry of c.details) {
         // let price = 0;
         let temp = '';
         let flag = true;
         for (let sub of entry.sub) {
            if (sub.selected) {
               if (flag) {
                  flag = false;
                  temp += sub.name[0].title;
               } else {
                  temp += ", " + sub.name[0].title;
               }
            }
         }
         if (temp != '') {
            out += entry.name[0].title + " : ";
            out += temp;
            out += "<br>";
         }

      }

      for (let entry of c.toppings) {
         if (entry.count - 0 > 0) {
            out += "  [" + entry.count + "] " + entry.title + " (";
            if (entry.price > 0) {
               out += "+";
            }
            out += (entry.price * entry.count) + ")";
            out += "<br>";
         }
      }
      // if (c.comments.trim() != '') {
      //    out += "Comment : " + c.comments;
      // }

      return out;
   }

   async presentAlertConfirm(order_id) {
      const alert = await this.alertController.create({
         header: 'ยกเลิกออเดอร์?',
         buttons: [
            {
               text: 'ยกเลิก',
               role: 'cancel',
               cssClass: 'secondary',
               handler: (count) => {
                  // null
               }
            }, {
               text: 'ตกลง',
               handler: () => {
                  // some code update API 
                  this.api.postData('Delivery_order/updateConfirmOrder', { order_id: order_id }).then((data:any)=>{
                     this.api.fb_set('data_pay');
                  })
                  this.close()
               }
            }
         ]
      });
      await alert.present();
   }

   confirmOrder(cf, cart_data, total) {
      if (cf) {
         console.log(1);
      } else {
         console.log(0);
      }
      this.close()
   }

   close() {
      this.modalCtrl.dismiss()
   }


}



