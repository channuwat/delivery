import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { OrderDetailPagePage } from './order-detail-page/order-detail-page.page';
import { Storage } from '@ionic/storage';
import { WabapiService } from '../wabapi.service';

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.page.html',
  styleUrls: ['./order-page.page.scss'],
})
export class OrderPagePage implements OnInit {

  public res_id = 0
  public res_uri = ''
  constructor(
    public modalController: ModalController,
    public store: Storage,
    public api: WabapiService,
    private ref: ChangeDetectorRef
  ) {
    this.store.get('restaurant').then(data => {
      this.res_id = data.id_res_auto
      this.res_uri = data.uri
      this.loadOrder(2) // user login
      this.loadOrderHistory(2) // user login
    })
  }

  ngOnInit() {
    this.api.fb_value('data_pay', () => {
      this.loadOrder(2) // user login
      this.loadOrderHistory(2) // user login
    })
    
  }


  ionViewWillEnter() {
    this.setConfirmOrder();
  }

  public order_list = []
  loadOrder(user_id) {
    this.api.getData('Delivery_order/getOrder/' + user_id + '/' + this.res_id).then((order: any) => {
      this.order_list = order
      this.order_list.forEach(element => {
        if (element.status == 0) {
          element['status_txt'] = 'ยกเลิก'
        } else if (element.status == 1) {
          element['status_txt'] = 'ได้รับแล้ว'
        }
        else if (element.status == 2) {
          element['status_txt'] = 'ร้านรับออเดอร์แล้ว'
        } else if (element.status == 3){
          element['status_txt'] = 'ส่งออเดอร์ให้ร้าน'
        }else{
          element['status_txt'] = 'กำลังออกส่ง'
        }
      });
      this.ref.detectChanges()
    })
  }

  public order_history = []
  loadOrderHistory(user_id) {
    this.api.getData('Delivery_order/getOrderHistory/' + user_id + '/' + this.res_id).then((order: any) => {
      this.order_history = order
      this.order_history.forEach(element => {
        if (element.status == 0) {
          element['status_txt'] = 'ยกเลิก'
        } else if (element.status == 1) {
          element['status_txt'] = 'ได้รับแล้ว'
        }
        else if (element.status == 2) {
          element['status_txt'] = 'ร้านรับออเดอร์แล้ว'
        } else if (element.status == 3){
          element['status_txt'] = 'ส่งออเดอร์ให้ร้าน'
        }else{
          element['status_txt'] = 'กำลังออกส่ง'
        }
      });
      this.ref.detectChanges()
    })
  }


  public wait_order = true
  public total = 0
  setConfirmOrder() {
    // เช็คว่ามีออเดอร์รอยืนยันมั้ย
    // จะเปิดออเดอร์ใหม่ได้ต้องยืนยันการสั่งซื้อก่อน
    this.store.get('restaurant').then((res_data: any) => {
      this.total = 0
      this.store.get('cart_' + res_data.id_res_auto).then((cart_data: any) => {
        this.wait_order = true
        if (cart_data == null) {
          this.wait_order = false
        } else {
          cart_data.forEach(element => {
            this.total += element.price * element.count
          });
        }
      })
    })
  }

  async modalOrder(order_id) {
    const modal = await this.modalController.create({
      component: OrderDetailPagePage,
      componentProps: {
        'order_id': order_id
      }
    })
    modal.onWillDismiss().then(() => {
      this.setConfirmOrder();
    })
    modal.present();
  }

  public type_order = true;
  Order(type) {
    this.type_order = true;
    if (!type) {
      this.type_order = false;
    }
  }
}
