import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderPagePage } from './order-page.page';

const routes: Routes = [
  {
    path: '',
    component: OrderPagePage
  },
  {
    path: ':order_id',
    loadChildren: () => import('./order-detail-page/order-detail-page.module').then( m => m.OrderDetailPagePageModule)
  },
  {
    path: 'order-map',
    loadChildren: () => import('../delivery-modal/order-map/order-map.module').then( m => m.OrderMapPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderPagePageRoutingModule {}
