import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderPagePageRoutingModule } from './order-page-routing.module';

import { OrderPagePage } from './order-page.page';
import { OrderDetailPagePage } from './order-detail-page/order-detail-page.page';
import { OrderDetailPagePageModule } from './order-detail-page/order-detail-page.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderPagePageRoutingModule,
    OrderDetailPagePageModule,
  ],
  entryComponents:[OrderDetailPagePage],
  declarations: [OrderPagePage],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class OrderPagePageModule {}
