import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeliveryModalPageRoutingModule } from './delivery-modal-routing.module';

import { DeliveryModalPage } from './delivery-modal.page';
import { OptionModalPage } from './option-modal/option-modal.page';
import { OptionModalPageModule } from './option-modal/option-modal.module';
import { OrderDetailModalPageModule } from './order-detail-modal/order-detail-modal.module';
import { OrderDetailModalPage } from './order-detail-modal/order-detail-modal.page';
import { GoogleMaps } from '@ionic-native/google-maps/ngx';
import { OrderMapPage } from './order-map/order-map.page';
import { OrderMapPageModule } from './order-map/order-map.module';
import { OrderDetailPagePage } from '../order-page/order-detail-page/order-detail-page.page';
import { OrderDetailPagePageModule } from '../order-page/order-detail-page/order-detail-page.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeliveryModalPageRoutingModule,
    OptionModalPageModule,
    OrderDetailModalPageModule,
    OrderMapPageModule,
    OrderDetailPagePageModule
  ],
  declarations: [DeliveryModalPage],
  entryComponents:[OptionModalPage,OrderDetailModalPage,OrderMapPage,OrderDetailPagePage],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  providers:[
    GoogleMaps
  ]
})
export class DeliveryModalPageModule {}
