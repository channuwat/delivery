import { Component, OnInit, Input, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { NavParams, Platform } from '@ionic/angular';
import { ModalController, IonSlides } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { WabapiService } from '../wabapi.service';
import { Storage } from '@ionic/storage';
import { OptionModalPage } from './option-modal/option-modal.page';
import { OrderDetailModalPage } from './order-detail-modal/order-detail-modal.page';
import { OrderMapPage } from './order-map/order-map.page';

@Component({
  selector: 'app-delivery-modal',
  templateUrl: './delivery-modal.page.html',
  styleUrls: ['./delivery-modal.page.scss'],
})
export class DeliveryModalPage implements OnInit {
  @ViewChild('sliderRef', { static: true }) protected slides: ElementRef<IonSlides>;
  public cart = [];
  public index_active = 0;
  @Input() Res_id: number;
  public Res = this.navParams.get('Res_id')

  constructor(
    public navParams: NavParams,
    public service: WabapiService,
    public s: Storage,
    public changeDetector: ChangeDetectorRef,
    public route: ActivatedRoute,
    public modalController: ModalController,
    public platform: Platform
  ) {

  }

  ngOnInit() {
    this.loadFoods(this.Res)
    this.setCart()
    this.isCart()
  }


  public group_foods = [];
  public foods = [];
  loadFoods(res_id) {
    this.s.get('store_group_foods').then((group_data: any) => {
      if (group_data === null) {
        this.service.getData('API_emp_app3/get_all_product/' + res_id).then((foods: any) => {
          this.group_foods = foods.group;
          this.foods = foods.foods;
          this.s.set('store_group_foods', foods.group)
          this.s.set('store_foods', foods.foods)
        })
      } else {
        this.group_foods = group_data
        this.s.get('store_foods').then((foods_data: any) => {
          if (foods_data === null) {
            this.foods = []
          } else {
            this.foods = foods_data
          }
        })
      }
      this.isCart()
    })
  }

  public is_cart = 0;
  isCart() {
    this.is_cart = 0;
    if (this.cart.length > 0) {
      this.is_cart = this.cart.length
    }
  }
  count_cart(status) {
    if (status) {
      var total = 0
      this.cart.forEach(cart => {
        total += Number(cart.price * cart.count)
      });
      return total
    } else {
      var count = 0
      this.cart.forEach(cart => {
        count += Number(cart.count)
      });
      return count
    }
  }

  async slidePrev(): Promise<void> {
    await this.slides.nativeElement.slidePrev();
  }

  group_click(sliderRef, i) {
    sliderRef.slideTo(i)
    this.index_active = i;
  }
  slideDidChange(sliderRef) {
    sliderRef.getActiveIndex().then(index => {
      this.index_active = index;
    });
  }

  async slideNext(): Promise<void> {
    await this.slides.nativeElement.slideNext();
  }

  async getActiveIndex(): Promise<number> {
    return this.slides.nativeElement.getActiveIndex();
  }


  setCart() {
    this.s.get('cart_' + this.Res).then((v) => {
      if (v == null) {
        this.get_count(null)
      } else {
        this.cart = v
      }
    })
  }

  get_food(group) {
    return this.foods.filter(f => f.f_group == group.fg_id);
  }

  remove_product(p) {
    let flag = true;
    this.cart.forEach((val, key) => {
      if (val.f_id == p.f_id && flag) {
        flag = false;
        if (val.count == 1) {
          this.cart.splice(key, 1);
        } else {
          val.count--;
        }

      }
    });
    this.s.set("cart_" + this.Res, this.cart).then(() => {
      this.s.get("cart_" + this.Res).then((check_cart: any) => {
        if (check_cart.length < 1) {
          this.s.remove("cart_" + this.Res);
        }
      })
      this.isCart()
    })

  }

  is_change(food) {
    var change = false;
    if (food.toppings.length - 0 !== 0) {
      let count = 0;
      for (let i = 0; i < food.toppings.length; i++) {
        count += food.toppings[i].count - 0;
      }
      if (count !== 0) {
        return true;
      }
    }

    for (let key = 0; key < food.details.length; key++) {
      let val = food.details[key];
      for (let key2 = 0; key2 < val.sub.length; key2++) {
        let val2 = val.sub[key2];
        if (val2.default_val && !val2.selected) {
          change = true;
        } else if (!val2.default_val && val2.selected) {
          change = true;
        }
      };
    };
    if (food.comments !== "") {
      change = true;
    }
    return change;
  };

  check_food(f_id, val) {
    let out = { count: 1, index: -1 };
    for (let key = 0; key < val.length; key++) {
      if (val[key].f_id - 0 === f_id - 0) {
        if (!this.is_change(val[key])) {
          out.count = val[key].count - 0 + 1;
          out.index = key;
        }
      }
    }
    return out;
  };

  // ------------- เพิ่มราายการสินค้า ------------- //
  add_product(data) {
    if (data === undefined) {
      return undefined
    }
    var temp = [];
    this.s.get('cart_' + this.Res).then((v) => {
      if (!v) {
        v = [];
      }
      temp = v;
      let a = this.check_food(data.f_id, temp);
      if (a.index === -1) {
        temp.push(data);
      } else {
        temp[a.index].count = a.count;
      }
      let count = 0;
      for (let i = 0; i < temp.length; i++) {
        count += temp[i].count - 0;
      }
      this.s.set('cart_' + this.Res, temp);
      setTimeout(() => {
        this.s.get('cart_' + this.Res).then((order) => {
          this.cart = order;
        });
      }, 10);
      //   });
      // });
    })
    this.isCart()
  }

  get_count(f_id) {
    /**
     * เช็คว่ามีการลบจำนวนจาก order detail จนไม่มีรายการหรือไม่
     * ใช่ จำนวนสินค้าจะว่าง
     * ไม่ จำนวนสินค้าจะปรับ เพิ่ม-ลด ตามหน้า order detail
     */
    let count = 0;
    if (f_id == null) {
      this.cart = [];
    } else {
      this.cart.forEach((val) => {
        if (val.f_id == f_id) {
          count += val.count;
        }
      });
      if (count > 0) {
        return count;
      } else {
        return '';
      }
    }
  }

  async openOption(id_res, p, index) {
    let temp = JSON.parse(JSON.stringify(p));
    const modal = await this.modalController.create({
      component: OptionModalPage,
      componentProps: { id_res: id_res, data: temp, index: index },
      backdropDismiss: false
    });
    modal.onDidDismiss()
      .then((data) => {
        
        this.setCart()
      });
    modal.present();
  }

  dismiss(error_txt) {
    this.modalController.dismiss(error_txt)
  }
}
