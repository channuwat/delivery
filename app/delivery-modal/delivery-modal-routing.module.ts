import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeliveryModalPage } from './delivery-modal.page';

const routes: Routes = [
  {
    path: '',
    component: DeliveryModalPage
  },
  {
    path: 'order-detail-modal',
    loadChildren: () => import('./order-detail-modal/order-detail-modal.module').then( m => m.OrderDetailModalPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeliveryModalPageRoutingModule {}
