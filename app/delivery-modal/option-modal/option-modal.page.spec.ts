import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OptionModalPage } from './option-modal.page';

describe('OptionModalPage', () => {
  let component: OptionModalPage;
  let fixture: ComponentFixture<OptionModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OptionModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
