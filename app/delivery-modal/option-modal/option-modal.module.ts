import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OptionModalPageRoutingModule } from './option-modal-routing.module';

import { OptionModalPage } from './option-modal.page';
import { TranslateModule } from '@ngx-translate/core';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OptionModalPageRoutingModule,
    TranslateModule.forChild(),
    IonicStorageModule.forRoot(),
  ],
  declarations: [OptionModalPage]
})
export class OptionModalPageModule {}
