import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OptionModalPage } from './option-modal.page';

const routes: Routes = [
  {
    path: '',
    component: OptionModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OptionModalPageRoutingModule {}
