import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { WabapiService } from 'src/app/wabapi.service';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { OrderDetailModalPage } from '../order-detail-modal/order-detail-modal.page';
declare var google;

@Component({
  selector: 'app-order-map',
  templateUrl: './order-map.page.html',
  styleUrls: ['./order-map.page.scss'],
})
export class OrderMapPage implements OnInit {

  public res_id = ''
  constructor(
    public api: WabapiService,
    public modalCtrl: ModalController,
    public store: Storage,
    private geolocation: Geolocation,
  ) {
    this.store.get('restaurant').then((data: any) => {
      this.res_id = data.id_res_auto
    })
    this.loadMap()
  }

  ngOnInit() {
  }

  @ViewChild('mapElement', { static: false }) mapNativeElement: ElementRef;
  public latitude: any;
  public longitude: any;
  loadMap() {
    this.geolocation.getCurrentPosition().then((resp) => {


      this.store.get('localtion_tmp').then((local_data: any) => {
        if (local_data == null) {
          this.latitude = resp.coords.latitude;
          this.longitude = resp.coords.longitude;
        } else {
          this.latitude = local_data.lat
          this.longitude = local_data.long
        }
      }).then(() => {

        const pos = {
          lat: this.latitude,
          lng: this.longitude
        };

        console.log(pos);
        
        const map = new google.maps.Map(this.mapNativeElement.nativeElement, {
          center: pos,
          zoom: 17
        });

        map.setCenter(pos);
        const icon = {
          url: 'assets/icon/icon_map/u.png', // image url
          scaledSize: new google.maps.Size(50, 50), // scaled size
        };
        const marker = new google.maps.Marker({
          position: pos,
          map: map,
          title: 'Hello World!',
          icon: icon
        });
        map.addListener('idle', (e) => {
          marker.setPosition(map.getCenter());
          this.latitude = marker.getPosition().lat();
          this.longitude = marker.getPosition().lng();
        });
      })

    }).catch((error) => {
      console.log('Error getting location', error);
    })
  }

  public calRote = {
    distance: {
      text: ''
    },
    duration: {
      text: ''
    },
    price: -1,
    text: '',
    lat: 0,
    long: 0
  }
  public condition_close = { txt_condition: 'order_item_list', route: null }
  async calDelivery() {
    console.log({ res_id: this.res_id, local: { lat: this.latitude, long: this.longitude } });
    
    await this.api.postData('Store/calDelivery', { res_id: this.res_id, local: { lat: this.latitude, long: this.longitude } }).then((cal_data: any) => {
      console.log(cal_data);
      
      if (cal_data.price != -1) {
        this.store.set('localtion_tmp', { lat: this.latitude, long: this.longitude })
        this.close({ txt_condition: 'order_detail_modal', route: cal_data })
      }
      else {
        this.api.Toast('เกินระยะจัดส่งโปรดลองใหม่อีกครั้ง')
      }
    })
  }

  close(data) {
    this.modalCtrl.dismiss(data)
  }
}
