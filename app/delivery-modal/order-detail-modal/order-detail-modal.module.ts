import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderDetailModalPageRoutingModule } from './order-detail-modal-routing.module';

import { OrderDetailModalPage } from './order-detail-modal.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    OrderDetailModalPageRoutingModule
  ],
  declarations: [OrderDetailModalPage],
  providers:[Geolocation]
})
export class OrderDetailModalPageModule {}
