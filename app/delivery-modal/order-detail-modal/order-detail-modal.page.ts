import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef, Input } from '@angular/core';
import { ModalController, AlertController, PickerController, NavController, ToastController, IonInput } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { OptionModalPage } from '../option-modal/option-modal.page';
import { PickerOptions } from "@ionic/core";
import { WabapiService } from '../../wabapi.service';
import { File } from '@ionic-native/file/ngx';

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { GoogleMapsEvent, LatLng } from '@ionic-native/google-maps/ngx';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { visitValue } from '@angular/compiler/src/util';
import { HttpClient } from '@angular/common/http';
declare var google;


@Component({
  selector: 'app-order-detail-modal',
  templateUrl: './order-detail-modal.page.html',
  styleUrls: ['./order-detail-modal.page.scss'],
})
export class OrderDetailModalPage implements OnInit {
  detailForm: FormGroup;
  public load = true
  public amout = []
  public res_id = ''
  constructor(
    public api: WabapiService,
    public modalCtrl: ModalController,
    public store: Storage,
    public changeDetector: ChangeDetectorRef,
    public alertController: AlertController,
    private geolocation: Geolocation,
    public toastCtrl: ToastController,
    public file: File,
    public http: HttpClient
  ) {

    this.detailForm = new FormGroup({
      'location_pick': new FormControl('', Validators.compose([
        Validators.required
      ])),
      'local_remark': new FormControl('', Validators.compose([])),
      'new_address': new FormControl('', Validators.compose([])),
      'bank_pick': new FormControl('', Validators.compose([
        Validators.required
      ])),
    });

    this.detailForm.setValue({
      location_pick: '0',
      local_remark: '',
      new_address: '',
      bank_pick: ''
    });

    this.store.get('restaurant').then((data: any) => {
      this.res_id = data.id_res_auto
      this.setBank(this.res_id)  // paramitor is restaurant id
    })
  }


  public imagePath: any = '';
  public url = '';
  async add_slip(event) {
    event.preventDefault();
    let element: HTMLElement = document.getElementById('slipinput') as HTMLElement;
    element.click();
  }
  public formData = new FormData();
  changeListener(event): void {

    this.formData.append('avatar', event.target.files[0]);
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.url = event.target.result;
    }
    reader.readAsDataURL(event.target.files[0]);

    // this.http.post(this.api.baseUrlApi + '../Delivery_order/uploadfile', this.formData).subscribe((res: any) => {
    //   console.log(res);
    //   this.url = res.url;
    // });

  }

  @Input() route_data: any;

  public route = {}
  ngOnInit() {
    console.log(this.route_data);
    this.route = this.route_data
    this.delivery = this.route_data.price

  }


  @ViewChild('remark', { static: false }) localRemark: IonInput
  ionViewWillEnter() {
    setTimeout(() => {
      this.localRemark.setFocus()
    }, 150);
    this.setConfirmOrder()
  }

  ionViewWillLeave() {
    this.load = false
  }

  public type_pay = 0
  public res_bank = []
  setBank(res_id) {
    this.api.getData('Delivery_order/getPayment/' + res_id).then((data: any) => {
      this.res_bank = data
    })
  }

  public wait_order =
    {
      res: { name: '' },
      cus: { id_cus: 1, name: 'delta_user' },
      address: { lat: 16.4547203, long: 102.8408059, name: '15/5 หมู่ 13 ถนน บ้านดอนหญ้านาง ตำบลในเมือง อำเภอเมืองขอนแก่น ขอนแก่น 40000' },
      data_detail: [],
    }
  setConfirmOrder() {
    this.store.get('restaurant').then((data_res: any) => {
      var has_list = 0;
      this.store.get('cart_' + data_res.id_res_auto).then(data_detail => {
        this.wait_order.res = data_res;
        this.wait_order.data_detail = data_detail;
        // set count item list order
        this.wait_order.data_detail.forEach((element, index) => {
          this.amout[index] = 0
          this.additem(element.count, index)
          has_list += element.count
        });

      }).then(() => {
        if (has_list == 0) {
          this.store.remove('cart_' + data_res.id_res_auto)
        }
      })
    })
  }

  public location = [
    { local_id: null, local_user_id: null, local_name: '', local_remark: '', local_lat: null, local_long: null }
  ]

  public save_new_map = false
  public edit_map = false
  public data_local = {}
  public location_pick = { local_id: '0', local_user_id: '0', local_name: '', local_remark: '', local_lat: null, local_long: null }

  public chk_address = false
  public new_address = {}
  chkNewaddress() {
    /**
     * false  = check box ยังไม่โดนติ๊ก
     * true = check box โดนติ๊กเเล้ว
     */
    if (this.chk_address) {
      this.chk_address = false
    } else {
      this.chk_address = true
    }
  }

  public total = 0
  public delivery = 0
  additem(add, index) {

    if ((this.amout[index] + add) < 1) {
      this.presentAlertConfirm(index, this.wait_order.data_detail[index].name)
    } else {
      this.amout[index] = this.amout[index] + add
      this.wait_order.data_detail[index].count = this.amout[index]
    }
    this.store.get('restaurant').then((data: any) => {
      let new_order = this.wait_order.data_detail
      this.store.remove('cart_' + data.id_res_auto)
      this.store.set('cart_' + data.id_res_auto, new_order)
    })
  }
  getTotal(type) {
    this.total = 0
    this.delivery = this.route_data.price

    this.wait_order.data_detail.forEach(element => {
      this.total += element.price * element.count
    });

    if (this.wait_order.data_detail.length <= 0) {
      this.total = 0
      this.delivery = 0
    } else {
      this.delivery = this.route_data.price
      if (this.route_data.price == -1) {
        this.delivery = 0
      }
    }

    if (type == 'food') {
      return this.total
    } else if (type == 'delivery') {
      return this.delivery
    } else {
      return this.total + this.delivery
    }

  }
  async presentAlertConfirm(re_index, name_item) {
    const alert = await this.alertController.create({
      header: 'ลบรายการ?',
      message: '<strong>ลบ : </strong>' + name_item,
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (count) => {
            this.amout[re_index] = 1
            this.wait_order.data_detail[re_index].count = this.amout[re_index]
          }
        }, {
          text: 'ตกลง',
          handler: () => {
            this.removeList(re_index)
          }
        }
      ]
    });
    await alert.present();
  }

  removeList(re_index) {
    var res: any
    this.store.get('restaurant').then(data_res => {
      res = data_res
    }).then(() => {
      let cart_tmp = []
      var index_tmp = 0
      this.store.get('cart_' + res.id_res_auto).then(list => {
        list.forEach((element, index) => {
          if (re_index != index) {
            cart_tmp[index_tmp] = element
            index_tmp++
          }
        });
      }).then(() => {
        this.store.remove('cart_' + res.id_res_auto)
        this.store.set('cart_' + res.id_res_auto, cart_tmp)
        this.setConfirmOrder()
      })
    })
  }

  get_html(c): String {
    let out: String = '';
    for (let entry of c.details) {
      // let price = 0;
      let temp = '';
      let flag = true;
      for (let sub of entry.sub) {
        if (sub.selected) {
          if (flag) {
            flag = false;
            temp += sub.name[0].title;
          } else {
            temp += ", " + sub.name[0].title;
          }
        }
      }
      if (temp != '') {
        out += entry.name[0].title + " : ";
        out += temp;
        out += "<br>";
      }

    }

    for (let entry of c.toppings) {
      if (entry.count - 0 > 0) {
        out += "  [" + entry.count + "] " + entry.title + " (";
        if (entry.price > 0) {
          out += "+";
        }
        out += (entry.price * entry.count) + ")";
        out += "<br>";
      }
    }
    if (c.comments.trim() != '') {
      out += "Comment : " + c.comments;
    }

    return out;
  }
  preSetOption(has_opt, indexS) {
    /**
     * เหตุที่ต้อง .then ซ้อนกันเพราะให้ทำงานรอกันไม่เช่นนั้นคำาั่งจะ error เพราะโปรเเกรมทำงานเร็วไป
     * เลยไม่มีข้อมูลส่งไป function
     */
    //---------------------------------------//
    // ถ้า false จะไม่มีหน้า option แสดง
    if (has_opt) {
      let id_res = '';
      let p = []
      let temp = []
      this.store.get('restaurant').then((data_res) => {
        id_res = data_res.id_res_auto
      }).then(() => {
        this.store.get('cart_' + id_res).then(data_cart => {
          data_cart.forEach((element, i) => {
            if (indexS == i) {
              p = element
            }
          });
        }).then(() => {
          temp = JSON.parse(JSON.stringify(p));
          this.openOption(id_res, temp, indexS)
        })
      })
    }
  }
  async openOption(id_res, temp, indexS) {
    const modal = await this.modalCtrl.create({
      component: OptionModalPage,
      componentProps: { id_res: id_res, data: temp, index: indexS }
    });
    modal.present();
  }

  public select_bank = true
  public pay = { type: 'cash', bank: {} }
  chkTypePay(type, copy) {

    /**
     * 0 = ปลายทาง
     * 1 = โอน
     * select_bank ถ้า true แสดงว่าเป็น เก็บปลายทาง ถ้า false คือ เเจ้งโอน
     */
    if (type == 0) {
      this.select_bank = true
      this.type_pay = 0
      this.pay = { type: 'cash', bank: {} }
    } else {
      this.select_bank = false
      this.type_pay = 1
      this.detailForm.patchValue({ bank_pick: this.res_bank[0].id_pay_type })
      let bank_select = this.detailForm.value.bank_pick
      this.res_bank.forEach(bank => {
        if (bank_select == bank.id_pay_type) {
          this.pay.type = 'transfer'
          this.pay.bank = bank
          this.copyMessage(bank.acount_number)
        }
      });
    }
  }
  public chageBank() {
    let bank_select = this.detailForm.value.bank_pick
    this.res_bank.forEach(bank => {
      if (bank_select == bank.id_pay_type) {
        this.pay.type = 'transfer'
        this.pay.bank = bank
        this.copyMessage(bank.acount_number)
      }
    });
  }
  public copyMessage(val: string) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.api.Toast('คัดลอกเลขบัญชีธนาคาร ' + val)
  }

  public pickGiveaway() {
    let element: HTMLElement = document.getElementById('giveaway') as HTMLElement;
    element.click()
  }
  public chk_giveaway = false
  chkGiveaway() {
    if (this.chk_giveaway) {
      this.chk_giveaway = false
    }
    else {
      this.chk_giveaway = true
    }


  }

  confirmOrder(cf, cart_data, data_local): void {
    if (cf) {
      let u_id = ''
      this.store.get('user').then((data: any) => {
        u_id = data.mm_id
        if (this.chk_address) {
          this.new_address =
          {
            new_address: this.detailForm.value.new_address,
            remark: this.detailForm.value.local_remark,
            lat: data_local.lat,
            long: data_local.long
          }
        } else {
          this.new_address = {}
        }

        // set data ก่อนนำส่ง API
        let api =
        {
          res: cart_data.res.id_res_auto,
          cus_id: u_id,
          cart: cart_data.data_detail,
          local: data_local,
          remark_local: this.detailForm.value.local_remark,
          new_dress: this.new_address,
          type_pay: this.pay,
          giveaway: this.chk_giveaway,
          img_pay: this.url
        }

        if (cart_data.data_detail.length > 0) {
          if (this.pay.type == 'transfer' && this.url == '') {
            this.api.Toast('โปรดแนบไฟล์การโอนชำระเงิน')
          } else {
            if (this.pay.type == 'transfer') {
              this.http.post(this.api.baseUrlApi + '../Delivery_order/uploadfile', this.formData).subscribe((res: any) => {
                console.log(res);
                api.img_pay = res.url
                this.api.postData('store/add_order', api).then((result: any) => {
                  this.api.fb_set('data_pay');
                  this.store.remove('cart_' + cart_data.res.id_res_auto)
                  this.store.remove('localtion_tmp')
                  this.close('add_success')
                });
              });
            } else {
              this.api.postData('store/add_order', api).then((result: any) => {
                this.api.fb_set('data_pay');
                this.store.remove('cart_' + cart_data.res.id_res_auto)
                this.store.remove('localtion_tmp')
                this.close('add_success')
              });
            }
          }
        } else {
          this.api.Toast('ไม่มีรายการอาหารไม่สามารถสั่งได้')
        }

      })

    } else {
      this.store.remove('cart_' + cart_data.res.id_res_auto)
      this.close('open_map')
    }

    // if (cf) {
    //   if (this.chk_address) {
    //     this.new_address =
    //     {
    //       new_address: this.detailForm.value.new_address,
    //       remark: this.detailForm.value.local_remark,
    //       lat: data_local.lat,
    //       long: data_local.long
    //     }
    //   } else {
    //     this.new_address = {}
    //   }

    //   // set data ก่อนนำส่ง API
    //   let api =
    //   {
    //     res: cart_data.res.id_res_auto,
    //     cus_id: 2,
    //     cart: cart_data.data_detail,
    //     local: data_local,
    //     remark_local: this.detailForm.value.local_remark,
    //     new_dress: this.new_address,
    //     type_pay: this.pay,
    //     giveaway: this.chk_giveaway,
    //     img_pay: this.url
    //   }

    //   console.log(api);
    //   if (this.pay.type == 'transfer' && this.url == '') {
    //     this.api.Toast('โปรดแนบไฟล์การโอนชำระเงิน')
    //   } else {
    //     this.api.postData('store/add_order', api).then((result: any) => {
    //       this.api.fb_set('data_pay');
    //       this.store.remove('cart_' + cart_data.res.id_res_auto)
    //       this.store.remove('localtion_tmp')
    //       this.close('add_success')
    //     });
    //   }

    // } else {
    //   this.store.remove('cart_' + cart_data.res.id_res_auto)
    //   this.close('open_map')
    // }

  }

  close(type_close) {
    this.modalCtrl.dismiss(type_close)
  }

  // dismiss() {
  //   // using the injected ModalController this page
  //   // can "dismiss" itself and optionally pass back data
  //   this.modalCtrl.dismiss({
  //     'dismissed': true
  //   });
  // }


}

