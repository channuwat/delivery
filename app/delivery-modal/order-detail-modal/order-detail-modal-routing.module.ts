import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderDetailModalPage } from './order-detail-modal.page';

const routes: Routes = [
  {
    path: '',
    component: OrderDetailModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderDetailModalPageRoutingModule {}
