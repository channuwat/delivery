import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QueueModalPage } from './queue-modal.page';

const routes: Routes = [
  {
    path: '',
    component: QueueModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QueueModalPageRoutingModule {}
