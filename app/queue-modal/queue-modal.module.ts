import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QueueModalPageRoutingModule } from './queue-modal-routing.module';

import { QueueModalPage } from './queue-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QueueModalPageRoutingModule
  ],
  declarations: [QueueModalPage]
})
export class QueueModalPageModule {}
