import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QueueModalPage } from './queue-modal.page';

describe('QueueModalPage', () => {
  let component: QueueModalPage;
  let fixture: ComponentFixture<QueueModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueueModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QueueModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
