import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  
  constructor(
    public router:Router,
    public store : Storage
    ) {
      // หากกลับมาหน้าหลักจะ remove storage restaurant,store_foods,store_group_foods หน้าที่เราเข้าไปดูทิ้ง
      this.store.remove('restaurant') 
      this.store.remove('store_foods')
      this.store.remove('store_group_foods')
   }

  ngOnInit() {
  }

  openRestaurant(uri){
    this.router.navigate(['',uri])
  }

}
