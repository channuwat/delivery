import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CartPage } from './cart.page';

const routes: Routes = [
  {
    path: '',
    component: CartPage
  },
  {
    path: 'cartdeliveryedit',
    loadChildren: () => import('./cartdeliveryedit/cartdeliveryedit.module').then( m => m.CartdeliveryeditPageModule)
  },
  {
    path: 'modal-editdetail',
    loadChildren: () => import('./modal-editdetail/modal-editdetail.module').then( m => m.ModalEditdetailPageModule)
  },
  {
    path: 'd-checkout',
    loadChildren: () => import('./d-checkout/d-checkout.module').then( m => m.DCheckoutPageModule)
  },
  {
    path: 'dcheckout',
    loadChildren: () => import('./dcheckout/dcheckout.module').then( m => m.DcheckoutPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CartPageRoutingModule {}
