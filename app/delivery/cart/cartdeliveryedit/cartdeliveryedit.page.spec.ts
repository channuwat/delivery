import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CartdeliveryeditPage } from './cartdeliveryedit.page';

describe('CartdeliveryeditPage', () => {
  let component: CartdeliveryeditPage;
  let fixture: ComponentFixture<CartdeliveryeditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartdeliveryeditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CartdeliveryeditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
