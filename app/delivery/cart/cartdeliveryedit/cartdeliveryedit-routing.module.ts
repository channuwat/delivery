import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CartdeliveryeditPage } from './cartdeliveryedit.page';

const routes: Routes = [
  {
    path: '',
    component: CartdeliveryeditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CartdeliveryeditPageRoutingModule {}
