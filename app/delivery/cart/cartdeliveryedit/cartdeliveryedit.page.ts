import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-cartdeliveryedit',
  templateUrl: './cartdeliveryedit.page.html',
  styleUrls: ['./cartdeliveryedit.page.scss'],
})
export class CartdeliveryeditPage implements OnInit {
  public res = "";
  public type = '';

  constructor(private route: ActivatedRoute, public router: Router, public navCtrl: NavController) { 
    this.res = route.snapshot.paramMap.get('res');
    let type = route.snapshot.paramMap.get('type');
  }

  ngOnInit() {
  }
  back() {
    // this.router.navigateByUrl("/" + this.res + "/delivery");
    this.navCtrl.navigateBack("/" + this.res + "/delivery/cart");
  }

}
