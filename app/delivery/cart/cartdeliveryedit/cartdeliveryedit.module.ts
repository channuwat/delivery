import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CartdeliveryeditPageRoutingModule } from './cartdeliveryedit-routing.module';

import { CartdeliveryeditPage } from './cartdeliveryedit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CartdeliveryeditPageRoutingModule
  ],
  declarations: [CartdeliveryeditPage]
})
export class CartdeliveryeditPageModule {}
