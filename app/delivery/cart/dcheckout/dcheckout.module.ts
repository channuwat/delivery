import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DcheckoutPageRoutingModule } from './dcheckout-routing.module';

import { DcheckoutPage } from './dcheckout.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DcheckoutPageRoutingModule
  ],
  declarations: [DcheckoutPage]
})
export class DcheckoutPageModule {}
