import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DcheckoutPage } from './dcheckout.page';

const routes: Routes = [
  {
    path: '',
    component: DcheckoutPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DcheckoutPageRoutingModule {}
