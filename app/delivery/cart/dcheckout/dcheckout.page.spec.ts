import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DcheckoutPage } from './dcheckout.page';

describe('DcheckoutPage', () => {
  let component: DcheckoutPage;
  let fixture: ComponentFixture<DcheckoutPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DcheckoutPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DcheckoutPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
