import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-d-checkout',
  templateUrl: './d-checkout.page.html',
  styleUrls: ['./d-checkout.page.scss'],
})
export class DCheckoutPage implements OnInit {
  public res = '';
  public type = '';
  public sum = '';
  public sum1: number = 0;
  public cart = [];
  public datad = {od_name:'', od_phone:'', od_address: ' ', od_price_all:0, foods: [], sum_all:0, od_price_send: '50', id_user:2, id_res:2}


  constructor(
    private route: ActivatedRoute,
    public navCtrl: NavController,
    private router: Router,
    private storage: Storage,
    private http: HttpClient
  ) { 
    this.res = this.route.snapshot.paramMap.get('res');
    this.type = this.route.snapshot.paramMap.get('type');

    this.storage.get('cart').then((v) => {
      this.cart = v;
      console.log(this.cart);
    });

    this.storage.get('sum').then(price => {
      this.sum = price.sum_all;
      this.sum1 =parseInt(this.datad.od_price_send) + parseInt(this.sum);
      console.log(this.sum1)
      console.log('price : ', this.sum);

    });
  }

  ngOnInit() {
  }


  back() {
    // this.router.navigateByUrl("/" + this.res + "/delivery");
    this.navCtrl.navigateBack("/" + this.res + "/delivery/cart");
  }

  
  save_order() {
    const data = new FormData();
    this.storage.get('cart').then((v) => {
      console.log(this.cart);
      this.datad.foods = v;
      data.append('datad', JSON.stringify(this.datad));
      //bdata.append('cart',JSON.stringify(this.cart));
      console.log('databookdddd :', this.datad);
      //console.log('cart : ',this.cart);

      // fdata.append('image', this.fileData, this.fileData.name);
      this.http.post('https://api.deltafood.co/reviews/order_delivery', data, {

      })
        .subscribe((events: any) => {
          console.log('testevent', events);
        })
    });
  }

}
