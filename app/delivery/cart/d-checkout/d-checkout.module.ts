import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DCheckoutPageRoutingModule } from './d-checkout-routing.module';

import { DCheckoutPage } from './d-checkout.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DCheckoutPageRoutingModule
  ],
  declarations: [DCheckoutPage]
})
export class DCheckoutPageModule {}
