import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DCheckoutPage } from './d-checkout.page';

const routes: Routes = [
  {
    path: '',
    component: DCheckoutPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DCheckoutPageRoutingModule {}
