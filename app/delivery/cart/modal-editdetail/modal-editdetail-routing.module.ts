import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalEditdetailPage } from './modal-editdetail.page';

const routes: Routes = [
  {
    path: '',
    component: ModalEditdetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalEditdetailPageRoutingModule {}
