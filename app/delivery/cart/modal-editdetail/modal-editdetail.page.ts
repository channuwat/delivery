import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, AlertController, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { WabapiService } from 'src/app/wabapi.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-modal-editdetail',
  templateUrl: './modal-editdetail.page.html',
  styleUrls: ['./modal-editdetail.page.scss'],
})
export class ModalEditdetailPage implements OnInit {

  public data;
  public cart_count = 0;
  public index = 0;
  public counts = [
    { number: 0 },
    { number: 1 },
    { number: 2 },
    { number: 3 },
    { number: 4 },
    { number: 5 },
    { number: 6 },
    { number: 7 },
    { number: 8 },
    { number: 9 }
  ];
  public currency: string = "บาท";
  public lang = "TH";
  public res = '';
  public type = '';

  constructor(
    public modalController: ModalController,
    private s: Storage, 
    public navParams: NavParams,
    public translate: TranslateService,
    // public navCtrl: AlertController,
    public service: WabapiService, 
    private route: ActivatedRoute,
    private router: Router,
    public navCtrl: NavController
  ) { 
    this.res = this.route.snapshot.paramMap.get('res');
    this.type = this.route.snapshot.paramMap.get('type');
    this.data = navParams.get('data');
    console.log(this.data);

    setInterval(() => {
      
      this.s.get('cart').then((v) => {
        let count = 0;
        for (let i = 0; i < v.length; i++) {
          count += v[i].count - 0;
        }
        this.cart_count = count;
      });
  }, 1000);
  }

  ngOnInit() {
  }
  closeeditmodal() {
    this.modalController.dismiss();
  }

  array_lang(text) {
    for (let val of text) {
      if (val.shot) {
        if (val.shot.toUpperCase() === this.lang) {
          return val.title;
        }
      }
    }
    return text[0].title;
  }
  addtopping(t){
    t.count++
    // this.tping.tcount++;

      
  }
  removetopping(t){
    if (t.count > 0) {
      t.count--;

    }
  }
  add() {
    this.data.count++;
  }
  remove() {
    if (this.data.count > 1) {
      this.data.count--;
    }

  }
  get_price_sub(del) {
    var price = 0;
    for (let val of del.sub) {
      if (val.selected) {
        price += val.price - 0;
      }
    }
    return price;
  };
  sum_item = function (f) {
    var total_price = f.price - 0;
    for (let val of f.toppings) {
      total_price += (val.price - 0) * val.count;
    };
    for (let detail of f.details) {
      total_price += this.get_price_sub(detail);
    };
    return total_price * f.count;
  };
  check_true(index) {
  }
  t = null;
  selected_change(index, data) {
    if (this.t !== null) {
      clearTimeout(this.t);
    }
    this.t = setTimeout(function () {
      var count_true = 0;
      console.log(data.details);
      for (let val of data.details[index].sub) {
        if (val.selected) {
          count_true++;
        }
      }

      if (count_true > data.details[index].number_max) {
        var c = count_true - data.details[index].number_max;
        for (var i = data.details[index].sub.length - 1; i >= 0; i--) {
          if (data.details[index].sub[i].selected) {
            data.details[index].sub[i].selected = false;
            c--;
            if (c === 0) {
              break;
            }
          }
        }
      } else if (count_true < data.details[index].number_min) {
        var c = data.details[index].number_min - count_true;
        for (var i = 0; i < data.details[index].sub.length; i++) {
          if (!data.details[index].sub[i].selected) {
            data.details[index].sub[i].selected = true;
            c--;
            if (c === 0) {
              break;
            }
          }
        }
      }
    }, 1000);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductDetailPage');
  }
  close(data) {
    this.s.get('cart').then((v) => {
      if (!v) { v = []; }
        if (v) {
          v[this.index] = data;        
          this.s.set('cart', v);
          this.modalController.dismiss();
        //  this.navCtrl.navigateBack('/' + this.res + "/delivery/cart");
         
        }
      });

  }
  // close(data) {
  //   this.s.get('data').then((val) => {
  //     this.s.get('cart').then((v) => {
  //       if (v) {
  //         v[this.index] = data;        
  //         this.s.set('cart', v);
  //         this.navCtrl.pop();
  //       }
  //     });
  //   });
  // }
  // cart() {
  //   this.navCtrl.push(CartPage);
  // }

  add_product(data) {
    console.log(data);
    var temp = [];
    this.s.get('cart').then((v) => {
      if (!v) { v = []; }
        temp = v;
        temp.push(data);
        this.cart_count = temp.length;
        this.s.set('cart', temp);
        this.translate.get("add").subscribe(add => {
          this.service.Toast(add);
          this.translate.get("success").subscribe(success => {
            this.service.Toast(success);
          });
        });


        console.log(temp);
      });
  }

  onSelectChange(data) {
    //   let sum = +data.price;
    //   for (let i of data.details) {
    //     for(let sub of i.sub){
    //       if(sub.selected){
    //          sum += +sub.price;

    //       }
    //     }
    //   }
    //   for (let i of data.toppings) {
    //     sum += +i.price * +i.count;
    //   }
    //   this.data.price_net = sum * +data.count;
  }
}
