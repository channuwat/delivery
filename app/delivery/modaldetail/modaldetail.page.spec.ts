import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModaldetailPage } from './modaldetail.page';

describe('ModaldetailPage', () => {
  let component: ModaldetailPage;
  let fixture: ComponentFixture<ModaldetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModaldetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModaldetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
