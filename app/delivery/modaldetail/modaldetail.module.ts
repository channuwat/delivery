import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModaldetailPageRoutingModule } from './modaldetail-routing.module';

import { ModaldetailPage } from './modaldetail.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    IonicModule,
    ModaldetailPageRoutingModule
  ],
  declarations: [ModaldetailPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
  
})
export class ModaldetailPageModule {}
