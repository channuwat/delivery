import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class WabapiService {
  public myData;

  public baseUrlApi = 'https://delivery.deltafood.co/api/index.php/';
  constructor(
    public http: HttpClient,
    private toast: ToastController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private storage: Storage
    , public firebase: AngularFireDatabase,
  ) {
    //
  }

  async  presentAlert(header, message) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['OK']
    });
    return await alert.present();
  }

  async Toast(text) {
    const toast = await this.toast.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }

  on_loading() {

    this.on_loading2('กรุณารอซักครู่');

  }
  async  on_loading2(wait) {
    let loading = await this.loadingCtrl.create({
      message: wait,
      spinner: 'crescent',
      duration: 1000
    });
    return await loading.present();
  }

  async error_toast() {
    const toast = await this.toast.create({
      message: "Error",
      duration: 3000
    });
  }

  getUser(user_id){
    this.getData('API_user/getUser/'+user_id).then((data_user:any)=>{
      this.storage.set('user',data_user)
    });
  }

  fb_value(child, func) {
    this.storage.get('restaurant').then((data: any) => {
      if (data) {
        let a = this.firebase.database
          .ref(data.id_res_auto + "_" + data.code + "/" + child).on("value", func);
      }
    });
  }

  fb_set(child) {
    this.storage.get('restaurant').then((data: any) => {
      if (data) {
        this.firebase.database
          .ref(data.id_res_auto + "_" + data.code + "/" + child).set(Math.ceil(Math.random()*100));
      }
    });
  }

  signFacebook(){
  }

  // POST Method
  postData(segment, objdata) {
    return new Promise((resolve, reject) => {
      var headers = new Headers();
      const requestOptions = new RequestOptions({ headers: headers });
      this.storage.get('setLanguage').then((result) => {
        this.http.post(this.baseUrlApi + segment + "?lang=" + result, JSON.stringify(objdata))
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            if (err.status == 0) {
              this.error_toast();
              reject(err);
            }
          });
      });
    });
  }
  // GET METHOD
  getData(segment) {
    return new Promise((resolve, reject) => {
      this.storage.get('setLanguage').then((result) => {
        result = 'th';
        this.http.get(this.baseUrlApi + segment + "?lang=" + result)
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            if (err.status == 0) {
              this.error_toast();
              reject(err);
            }
            reject(err);
          });
      });
    });
  }
}
