import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QueueTablePagePage } from './queue-table-page.page';

describe('QueueTablePagePage', () => {
  let component: QueueTablePagePage;
  let fixture: ComponentFixture<QueueTablePagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueueTablePagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QueueTablePagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
