import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QueueTablePagePageRoutingModule } from './queue-table-page-routing.module';

import { QueueTablePagePage } from './queue-table-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QueueTablePagePageRoutingModule
  ],
  declarations: [QueueTablePagePage]
})
export class QueueTablePagePageModule {}
