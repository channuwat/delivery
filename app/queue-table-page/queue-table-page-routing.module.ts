import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QueueTablePagePage } from './queue-table-page.page';

const routes: Routes = [
  {
    path: '',
    component: QueueTablePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QueueTablePagePageRoutingModule {}
