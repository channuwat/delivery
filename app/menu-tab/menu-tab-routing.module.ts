import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuTabPage } from './menu-tab.page';
import { TabsPage } from '../tabs/tabs.page';

const routes: Routes = [
  {
    path: ':res',
    component: MenuTabPage,
    children: [
      {
        path: '',
        loadChildren: () => import('../restaurant-page/restaurant-page.module').then(m => m.RestaurantPagePageModule)
      },
      {
        path: 'reviews_res',
        loadChildren: () => import('../restaurant-page/reviews-restaurant-page/reviews-restaurant-page.module').then(m => m.ReviewsRestaurantPagePageModule)
      },
      {
        path: 'queue_table',
        loadChildren: () => import('../queue-table-page/queue-table-page.module').then(m => m.QueueTablePagePageModule)
      },
      {
        path: 'queue',
        loadChildren: () => import('../queue-page/queue-page.module').then(m => m.QueuePagePageModule)
      },
      {
        path: 'order',
        loadChildren: () => import('../order-page/order-page.module').then(m => m.OrderPagePageModule)
      },
      {
        path: 'config',
        loadChildren: () => import('../restaurant-config-page/restaurant-config-page.module').then(m => m.RestaurantConfigPagePageModule)
      }
    ]
  },
  {
    path: 'res_home',
    children: [
      {
        path: ':res',
        loadChildren: () =>
          import('../restaurant-page/restaurant-page.module').then(m => m.RestaurantPagePageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo:'/home'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuTabPageRoutingModule { }
