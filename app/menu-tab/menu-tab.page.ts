import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WabapiService } from '../wabapi.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-menu-tab',
  templateUrl: './menu-tab.page.html',
  styleUrls: ['./menu-tab.page.scss'],
})
export class MenuTabPage implements OnInit {

  public res_uri = ''
  constructor(
    public router: Router,
    public service: WabapiService,
    public store: Storage
  ) {
    this.store.get('restaurant').then((res: any) => {
      this.res_uri = res.uri
    })
  }

  ngOnInit() {

  }

  click_tab(uri_1, uri_2) {
    if (uri_1 != '') {
      this.router.navigate(['/' + uri_1 + '/' + uri_2])
    } else {
      this.router.navigate(['/'])
    }
  }

}
