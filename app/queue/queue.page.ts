import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { WabapiService } from '../wabapi.service';

@Component({
  selector: 'app-queue',
  templateUrl: './queue.page.html',
  styleUrls: ['./queue.page.scss'],
})
export class QueuePage implements OnInit {

  public res = '';
  public type = '';
  public queue = {};
  public queue_count = 0;
  public q_id = 0;
  public dataq = { id_user: 2, id_res_auto: 2, name: '', phone: '', people: 0 };
  constructor(
    private route: ActivatedRoute,
    public navCtrl: NavController,
    private router: Router,
    private storage: Storage,
    private http: HttpClient,
    public service: WabapiService
  ) {

    this.res = this.route.snapshot.paramMap.get('res');
    this.type = this.route.snapshot.paramMap.get('type');

    this.service.getData('reviews/count_queue/2').then((queue: any) => {
    this.queue_count = queue;
      
        
  });
}


ngOnInit() {
}
back() {
  // this.router.navigateByUrl('/' + this.res);  
  this.navCtrl.navigateRoot('/' + this.res);
}
rawqueuse() {
  // this.router.navigateByUrl('/' + this.res+"/delivery/cart");
  this.navCtrl.navigateForward('/' + this.res + "/queue/rawqueue");

}



rawqueue() {
  const qdata = new FormData();

  qdata.append('dataq', JSON.stringify(this.dataq));
  //bdata.append('cart',JSON.stringify(this.cart));
  console.log('databookdddd :', this.dataq);
  //console.log('cart : ',this.cart);

  // fdata.append('image', this.fileData, this.fileData.name);
  this.http.post('https://api.deltafood.co/reviews/add_queue', qdata, {

  })
    .subscribe((events: any) => {
      console.log('testevent', events);
      this.q_id = events;
      this.storage.set('q_id',this.q_id)
      this.navCtrl.navigateForward('/' + this.res + "/queue/rawqueue");
    })
}
 
   


}
