import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RawqueuePage } from './rawqueue.page';

const routes: Routes = [
  {
    path: '',
    component: RawqueuePage
  },
  {
    path: 'queuecart',
    loadChildren: () => import('./queuecart/queuecart.module').then( m => m.QueuecartPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RawqueuePageRoutingModule {}
