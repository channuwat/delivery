import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RawqueuePageRoutingModule } from './rawqueue-routing.module';

import { RawqueuePage } from './rawqueue.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RawqueuePageRoutingModule
  ],
  declarations: [RawqueuePage]
})
export class RawqueuePageModule {}
