import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NavController, AlertController } from "@ionic/angular";
import { WabapiService } from "src/app/wabapi.service";
import { Storage } from "@ionic/storage";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-rawqueue",
  templateUrl: "./rawqueue.page.html",
  styleUrls: ["./rawqueue.page.scss"]
})
export class RawqueuePage implements OnInit {
  public gender;
  public show = false;
  public show_search = false;
  public res = "";
  public type = "";
  public group = [];
  public foods = [];
  public title = "";
  public queue = "";
  public queue_count = 0;
  public editdata = { id_res_auto: 2, q_id: 0, people: 0 };
  constructor(
    private route: ActivatedRoute,
    public navCtrl: NavController,
    private router: Router,
    public alertController: AlertController,
    public service: WabapiService,
    public storage: Storage,
    private http: HttpClient
  ) {
    this.res = this.route.snapshot.paramMap.get("res");
    this.type = this.route.snapshot.paramMap.get("type");

    this.load();
  }
  load() {
    this.storage.get("q_id").then(q_id => {
      this.service.getData("reviews/get_queue/" + q_id).then((result: any) => {
        this.queue = result;
        console.log("get_all_queue", this.queue);
      });
    });
  }
  ngOnInit() {}
  back() {
    // this.router.navigateByUrl("/" + this.res + "/delivery");
    this.navCtrl.navigateBack("/" + this.res + "/queue");
  }
  async numberq() {
    const alert = await this.alertController.create({
      header: "แก้ไขจำนวนที่นั่ง",
      inputs: [
        {
          name: "people",
          type: "number",
          placeholder: "จำนวนที่นั่ง"
        }
      ],
      buttons: [
        {
          text: "ยกเลิก",
          role: "cancel",
          cssClass: "secondary",
          handler: () => {
            console.log("Confirm Cancel");
          }
        },
        {
          text: "บันทึก",
          handler: val => {
            console.log(val);
            this.storage.get("q_id").then(q_id => {
              const qdata = new FormData();

              qdata.append("dataq",JSON.stringify({id_res_auto: 2,q_id: q_id,people: val.people})
              );
              this.http.post("https://api.deltafood.co/reviews/edit_people", qdata, {})
                .subscribe((events: any) => {
                  this.load();
                  console.log("testevent", events);
                });
            });
            console.log("Confirm Ok");
          }
        }
      ]
    });
    await alert.present();
  }
  qcart() {
    // this.router.navigateByUrl('/' + this.res+"/delivery/cart");
    this.navCtrl.navigateForward("/" + this.res + "/queue/rawqueue/queuecart");
  }
}
