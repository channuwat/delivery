import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RawqueuePage } from './rawqueue.page';

describe('RawqueuePage', () => {
  let component: RawqueuePage;
  let fixture: ComponentFixture<RawqueuePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RawqueuePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RawqueuePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
