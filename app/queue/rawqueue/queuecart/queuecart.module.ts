import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QueuecartPageRoutingModule } from './queuecart-routing.module';

import { QueuecartPage } from './queuecart.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QueuecartPageRoutingModule
  ],
  declarations: [QueuecartPage]
})
export class QueuecartPageModule {}
