import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QueuecartPage } from './queuecart.page';

const routes: Routes = [
  {
    path: '',
    component: QueuecartPage
  },
  {
    path: 'queueorder',
    loadChildren: () => import('./queueorder/queueorder.module').then( m => m.QueueorderPageModule)
  },
  {
    path: 'qmodaldetail',
    loadChildren: () => import('./qmodaldetail/qmodaldetail.module').then( m => m.QmodaldetailPageModule)
  },
  {
    path: 'qcart',
    loadChildren: () => import('./qcart/qcart.module').then( m => m.QcartPageModule)
  },
  {
    path: 'qeditmodaldetail',
    loadChildren: () => import('./qeditmodaldetail/qeditmodaldetail.module').then( m => m.QeditmodaldetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QueuecartPageRoutingModule {}
