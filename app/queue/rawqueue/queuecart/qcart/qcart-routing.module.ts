import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QcartPage } from './qcart.page';

const routes: Routes = [
  {
    path: '',
    component: QcartPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QcartPageRoutingModule {}
