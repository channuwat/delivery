import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QcartPage } from './qcart.page';

describe('QcartPage', () => {
  let component: QcartPage;
  let fixture: ComponentFixture<QcartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QcartPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QcartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
