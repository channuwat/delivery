import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QcartPageRoutingModule } from './qcart-routing.module';

import { QcartPage } from './qcart.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QcartPageRoutingModule
  ],
  declarations: [QcartPage]
})
export class QcartPageModule {}
