import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QeditmodaldetailPage } from './qeditmodaldetail.page';

describe('QeditmodaldetailPage', () => {
  let component: QeditmodaldetailPage;
  let fixture: ComponentFixture<QeditmodaldetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QeditmodaldetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QeditmodaldetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
