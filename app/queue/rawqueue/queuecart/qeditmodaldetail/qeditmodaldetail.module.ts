import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QeditmodaldetailPageRoutingModule } from './qeditmodaldetail-routing.module';

import { QeditmodaldetailPage } from './qeditmodaldetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QeditmodaldetailPageRoutingModule
  ],
  declarations: [QeditmodaldetailPage]
})
export class QeditmodaldetailPageModule {}
