import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QeditmodaldetailPage } from './qeditmodaldetail.page';

const routes: Routes = [
  {
    path: '',
    component: QeditmodaldetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QeditmodaldetailPageRoutingModule {}
