import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QueueorderPage } from './queueorder.page';

const routes: Routes = [
  {
    path: '',
    component: QueueorderPage
  },
  {
    path: 'queueorderedit',
    loadChildren: () => import('./queueorderedit/queueorderedit.module').then( m => m.QueueordereditPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QueueorderPageRoutingModule {}
