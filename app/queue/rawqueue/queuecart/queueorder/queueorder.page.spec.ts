import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QueueorderPage } from './queueorder.page';

describe('QueueorderPage', () => {
  let component: QueueorderPage;
  let fixture: ComponentFixture<QueueorderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueueorderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QueueorderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
