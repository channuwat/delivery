import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QueueordereditPageRoutingModule } from './queueorderedit-routing.module';

import { QueueordereditPage } from './queueorderedit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QueueordereditPageRoutingModule
  ],
  declarations: [QueueordereditPage]
})
export class QueueordereditPageModule {}
