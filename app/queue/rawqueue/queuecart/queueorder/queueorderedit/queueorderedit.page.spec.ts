import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QueueordereditPage } from './queueorderedit.page';

describe('QueueordereditPage', () => {
  let component: QueueordereditPage;
  let fixture: ComponentFixture<QueueordereditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueueordereditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QueueordereditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
