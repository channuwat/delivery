import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QueueordereditPage } from './queueorderedit.page';

const routes: Routes = [
  {
    path: '',
    component: QueueordereditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QueueordereditPageRoutingModule {}
