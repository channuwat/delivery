import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QueueorderPageRoutingModule } from './queueorder-routing.module';

import { QueueorderPage } from './queueorder.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QueueorderPageRoutingModule
  ],
  declarations: [QueueorderPage]
})
export class QueueorderPageModule {}
