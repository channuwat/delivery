import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QueuecartPage } from './queuecart.page';

describe('QueuecartPage', () => {
  let component: QueuecartPage;
  let fixture: ComponentFixture<QueuecartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueuecartPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QueuecartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
