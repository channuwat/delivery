import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, ToastController, AlertController, ModalController } from '@ionic/angular';
import { WabapiService } from 'src/app/wabapi.service';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { QmodaldetailPage } from './qmodaldetail/qmodaldetail.page';

@Component({
  selector: 'app-queuecart',
  templateUrl: './queuecart.page.html',
  styleUrls: ['./queuecart.page.scss'],
})
export class QueuecartPage implements OnInit {

  public gender;
  public show = false;
  public show_search = false;
  public res = '';
  public value = 0;
  public type = '';
  public group = [];
  public foods = [];
  public cart_count = 0;
  public zone_id = 0;
  public data = [];
  public title: String = "";
  public currency = "บาท";
  public push: any;
  constructor(
    private route: ActivatedRoute,
    public navCtrl: NavController,
    private router: Router,
    public service: WabapiService,
    public alertController: AlertController,
    public translate: TranslateService,
    public s: Storage,
    public modalController: ModalController
  ) {
    this.res = this.route.snapshot.paramMap.get('res');
    this.type = this.route.snapshot.paramMap.get('type');

    setInterval(() => {

      this.s.get('cart').then((v) => {
        let count = 0;
        for (let i = 0; i < v.length; i++) {
          count += v[i].count - 0;
        }
        this.cart_count = count;
      });
    }, 1000);
    service.getData('foods/get_group/' + this.res).then((result: any) => {
      console.log(result);
      this.group = result;
      if (this.group.length > 0) {
        this.title = this.group[0].name;
        this.get_product(this.group[0].fg_id);
      }
    });
  }

  ngOnInit() {
  }
  back() {
    // this.router.navigateByUrl("/" + this.res + "/delivery");
    this.navCtrl.navigateBack("/" + this.res + "/queue/rawqueue");
  }
  async qmodaldetail(f) {
    const modal = await this.modalController.create({
      component: QmodaldetailPage,
      cssClass: 'my-custom-modal-css2',
      componentProps: { data: f }
    });
    await modal.present();
    // console.log(f);
  }
  select_group(g) {
    console.log(g);
    this.get_product(g.fg_id);
    this.title = g.name;
    this.show = false;
  }
  get_product(fg_id: string) {
    let load = this.service.on_loading();
    this.service.getData('foods/get_product/' + fg_id).then((result: any) => {
      console.log(result);
      this.foods = result;


    });
  }
  qcart() {
    // this.router.navigateByUrl('/' + this.res+"/delivery/cart");
    this.navCtrl.navigateForward('/' + this.res + "/queue/rawqueue/queuecart/qcart");

  }
  add_product(data) {
    if (data.status - 0 === 0) {
      this.translate.get("oos_text").subscribe(oos_text => {
        this.service.Toast(oos_text);
      });
    } else {
      var temp = [];
      //this.s.get('data').then((val) => {
      //console.log(val);
      this.s.get('cart').then((v) => {
        if (!v) { v = []; }
        temp = v;
        console.log(v);
        let a = this.check_food(data.f_id, temp);
        console.log(a);
        if (a.index === -1) {
          data.count = 1;
          temp.push(data);
        } else {
          temp[a.index].count = a.count;
        }
        let count = 0;
        for (let i = 0; i < temp.length; i++) {
          count += temp[i].count - 0;
        }
        this.cart_count = count;
        this.s.set('cart', temp);
        this.translate.get("add").subscribe(add => {
          this.translate.get("success").subscribe(success => {
            this.service.Toast(add + ' ' + data.name + " " + success);
          });
        });
        console.log(temp);

      });
      2000
      //});
    }
  }
  is_change(food) {
    var change = false;
    if (food.toppings.length - 0 !== 0) {
      let count = 0;
      for (let i = 0; i < food.toppings.length; i++) {
        count += food.toppings[i].count - 0;
      }
      if (count !== 0) {
        return true;
      }
    }

    for (let key = 0; key < food.details.length; key++) {
      let val = food.details[key];
      for (let key2 = 0; key2 < val.sub.length; key2++) {
        let val2 = val.sub[key2];
        if (val2.default_val && !val2.selected) {
          change = true;
        } else if (!val2.default_val && val2.selected) {
          change = true;
        }
      };
    };
    if (food.comments !== "") {
      change = true;
    }
    return change;
  };
  check_food(f_id, val) {
    let out = { count: 1, index: -1 };
    for (let key = 0; key < val.length; key++) {
      if (val[key].f_id - 0 === f_id - 0) {
        console.log(val[key]);
        if (!this.is_change(val[key])) {
          out.count = val[key].count - 0 + 1;
          out.index = key;
        }
      }
    }

    return out;
  };
}
