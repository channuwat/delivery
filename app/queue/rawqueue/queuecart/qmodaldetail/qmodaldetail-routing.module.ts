import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QmodaldetailPage } from './qmodaldetail.page';

const routes: Routes = [
  {
    path: '',
    component: QmodaldetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QmodaldetailPageRoutingModule {}
