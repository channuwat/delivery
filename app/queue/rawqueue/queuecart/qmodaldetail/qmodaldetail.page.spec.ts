import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QmodaldetailPage } from './qmodaldetail.page';

describe('QmodaldetailPage', () => {
  let component: QmodaldetailPage;
  let fixture: ComponentFixture<QmodaldetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QmodaldetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QmodaldetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
