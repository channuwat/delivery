import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QmodaldetailPageRoutingModule } from './qmodaldetail-routing.module';

import { QmodaldetailPage } from './qmodaldetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QmodaldetailPageRoutingModule
  ],
  declarations: [QmodaldetailPage]
})
export class QmodaldetailPageModule {}
