import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QueuePage } from './queue.page';

const routes: Routes = [
  {
    path: '',
    component: QueuePage
  },
  {
    path: 'rawqueue',
    loadChildren: () => import('./rawqueue/rawqueue.module').then( m => m.RawqueuePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QueuePageRoutingModule {}
