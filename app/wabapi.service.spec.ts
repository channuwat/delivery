import { TestBed } from '@angular/core/testing';

import { WabapiService } from './wabapi.service';

describe('WabapiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WabapiService = TestBed.get(WabapiService);
    expect(service).toBeTruthy();
  });
});
