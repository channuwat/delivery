import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuController, ModalController } from '@ionic/angular';
import { WabapiService } from '../wabapi.service';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';

//import { AuthService, FacebookLoginProvider, SocialUser } from 'angularx-social-login';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  public reviews = [];
  public comments = [];
  public likes = [];
  public show = false;
  public tab = 1;
  public show_like = false;
  public res = '';
  public lcolor = false;
  user: '' //SocialUser;
  loggedIn: boolean;
  public like_post = 0;
  public type = '';
  datalike = { id_user: 1, id_post: 0 };
  data = { c_content: '', id_user: 1, id_post: 0 };
  public is_login = false;
  public res_data = { id_post_auto: '0', id_res_auto: '0', address: '', name: '', cover_url: '', logo_url: '', btn: { booking: false, delivery: false, queue: false }, detail: [], rec_pro: [] };
  
  public tenantIDFileName = '';
  public showclickimg = false;
  imageURI:any;
  imageFileName:any;
  public rating = 0;
  fileData: File = null;
  data1 = {p_title:'', p_content:'',p_rating:0,id_user:1,id_res:0};
  previewUrl:any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;

  constructor(
    //private authService: AuthService,
    private route: ActivatedRoute, 
    private menu: MenuController, 
    public modalController: ModalController, 
    private fb: Facebook,
    public service: WabapiService,
    private router: Router, 
    private http: HttpClient
    ) {
    this.res = this.route.snapshot.paramMap.get('res');
    this.type = this.route.snapshot.paramMap.get('type');
    //service.Toast('dsf');
    console.log(this.res);
    this.loaddata();
    if (this.type == 'dalivery') {
      this.router.navigateByUrl('/' + this.res + '/delivery');
    }
    if (this.type == 'booking') {
      this.router.navigateByUrl('/' + this.res + '/booking');
    }
    if (this.type == 'queue') {
      this.router.navigateByUrl('/' + this.res + '/queue');
    }
    if (this.type == 'review') {
      this.router.navigateByUrl('/' + this.res + '/review');
    }
    if (this.type == 'login') {
      this.router.navigateByUrl('/' + this.res + '/login');
    }


  }
  ngOnInit() {
    // this.authService.authState.subscribe((user) => {
    //   this.user = user;
    //   this.loggedIn = (user != null);
    //   console.log(this.user);
    // });

  }
  signInWithFB(): void {
    //this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    //this.authService.signOut();
  }
  facebook_login() {
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => console.log('Logged into Facebook!', res))
      .catch(e => console.log('Error logging into Facebook', e));


    this.fb.logEvent(this.fb.EVENTS.EVENT_NAME_ADDED_TO_CART);
  }
  loaddata() {
    this.service.getData('main/get_res/' + this.res + '?lang=th').then((result: any) => {
      console.log(result);
      if (result.flag) {
        this.data1.id_res=result.data.id_res_auto;
        this.data.id_post = result.data.id_post_auto;
        this.res_data = result.data;
        console.log(this.res_data);
        this.service.getData('reviews/get_reviews/' + this.res_data.id_res_auto + "/1").then((review: any) => {
          console.log(review);
          this.reviews = review;
        });
      } else {

      }
    })
  }
  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  openEnd() {
    this.menu.open('end');
  }

  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }
  delivery() {
    console.log("xxxx");
    this.router.navigateByUrl('/' + this.res + '/delivery');
  }
  booking() {
    console.log("xxx");
    this.router.navigateByUrl('/' + this.res + '/booking');
  }
  queue() {
    console.log("xxx");
    this.router.navigateByUrl('/' + this.res + '/queue');
  }
  login() {
    console.log("xxx");
    this.router.navigateByUrl('/' + this.res + '/login');
  }
  profile() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }
  review() {
    this.router.navigateByUrl('/' + this.res + '/review');
  }
  get_comment(idpost, i) {
    this.service.getData('reviews/get_comment/' + idpost).then((review: any) => {
      console.log(review);
      this.reviews[i].all_comment = review;
      this.reviews[i].comment = review.length;
    });
  }
  ConSubmit(data, i) {
    console.log(data);
    console.log(i);
    const fdata = new FormData();
    fdata.append('data', JSON.stringify({ id_post: data.id_post, c_content: data.c_content, id_user: 1 }));
    console.log(this.data);
    this.http.post('https://api.deltafood.co/reviews/c_comment', fdata, {
      reportProgress: true,
      observe: 'events'
    })
      .subscribe(events => {
        if (events.type === HttpEventType.UploadProgress) {
          console.log('Upload Progress: ' + Math.round(events.loaded / events.total) * 100 + '%');
        } else if (events.type === HttpEventType.Response) {
          console.log(events);
          data.c_content = '';
          this.get_comment(data.id_post, i);

        }
      })
  }

  likeSubmit(datalike, i) {
    console.log(datalike);
    console.log(i);
    const fdata = new FormData();
    fdata.append('data', JSON.stringify({ id_post: datalike.id_post, id_user: 1 }));
    console.log(this.data);
    this.http.post('https://api.deltafood.co/reviews/l_like', fdata, {
    })
      .subscribe((events: any) => {
        console.log(events);
        this.reviews[i].like = events.count;
      })

  }





  openFileBrowser (event: any){
    event.preventDefault();
    let element: HTMLElement = document.getElementById('uploadfile') as HTMLElement;
    element.click();
  }
  onFileChange (event: any){
    let files = event.target.files;
    this.tenantIDFileName = files[0].name;

    console.log(files)
  }
  
  set_rating(r){
    this.rating = r;
  }

  fileProgress(fileInput: any) {
      this.fileData = <File>fileInput.target.files[0];
      this.preview();

  }
  preview() {
    // Show preview 
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
 
    var reader = new FileReader();      
    reader.readAsDataURL(this.fileData); 
    reader.onload = (_event) => { 
      this.previewUrl = reader.result; 
      this.showclickimg =true;
    }
  }
   
  onSubmit() {
    const fdata = new FormData();
    this.data1.p_rating = this.rating;
    fdata.append('data1',JSON.stringify(this.data1));
    console.log(this.data1);
    fdata.append('image', this.fileData, this.fileData.name);
    this.http.post('https://api.deltafood.co/reviews/upload', fdata,{
      reportProgress: true,
      observe: 'events'   
    })
    .subscribe(events => {
      if(events.type === HttpEventType.UploadProgress) {
        console.log('Upload Progress: ' + Math.round(events.loaded / events.total) * 100 + '%');
      } else if(events.type === HttpEventType.Response) {
        // console.log(events);
      }
    })
}
}
