import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HeaderDefaultPage } from './header-default.page';

const routes: Routes = [
  {
    path: '',
    component: HeaderDefaultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HeaderDefaultPageRoutingModule {}
