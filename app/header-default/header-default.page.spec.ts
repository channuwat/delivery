import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HeaderDefaultPage } from './header-default.page';

describe('HeaderDefaultPage', () => {
  let component: HeaderDefaultPage;
  let fixture: ComponentFixture<HeaderDefaultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderDefaultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HeaderDefaultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
