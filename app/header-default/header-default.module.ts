import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HeaderDefaultPageRoutingModule } from './header-default-routing.module';

import { HeaderDefaultPage } from './header-default.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HeaderDefaultPageRoutingModule
  ],
  declarations: [HeaderDefaultPage]
})
export class HeaderDefaultPageModule {}
