import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: '',
    loadChildren: () => import('../app/menu-tab/menu-tab.module').then(m => m.MenuTabPageModule)
  },
  {
    path: 'order-page',
    loadChildren: () => import('./order-page/order-page.module').then( m => m.OrderPagePageModule)
  },
  {
    path: 'all-order-page',
    loadChildren: () => import('./all-order-page/all-order-page.module').then( m => m.AllOrderPagePageModule)
  },
  {
    path: 'home-config-page',
    loadChildren: () => import('./home-config-page/home-config-page.module').then( m => m.HomeConfigPagePageModule)
  },
  {
    path: 'config-address',
    loadChildren: () => import('./restaurant-config-page/config-address/config-address.module').then( m => m.ConfigAddressPageModule)
  },
  {
    path: 'restaurant-config-page',
    loadChildren: () => import('./restaurant-config-page/restaurant-config-page.module').then( m => m.RestaurantConfigPagePageModule)
  },
  {
    path: 'login-modal',
    loadChildren: () => import('./login-regis/login-modal/login-modal.module').then( m => m.LoginModalPageModule)
  },
  {
    path: 'regis-modal',
    loadChildren: () => import('./login-regis/regis-modal/regis-modal.module').then( m => m.RegisModalPageModule)
  },
  {
    path: 'otp-modal',
    loadChildren: () => import('./login-regis/otp-modal/otp-modal.module').then( m => m.OtpModalPageModule)
  },
  {
    path: 'queue-modal',
    loadChildren: () => import('./queue-modal/queue-modal.module').then( m => m.QueueModalPageModule)
  }

  // {
  //   path: ':res/delivery',
  //   loadChildren: () => import('./delivery/delivery.module').then(m => m.DeliveryPageModule)
  // },
  // {
  //   path: ':res/review',
  //   loadChildren: () => import('./review/review.module').then(m => m.ReviewPageModule)
  // },
  // {
  //   path: ':res/delivery/cart/',
  //   loadChildren: () => import('./delivery/cart/cart.module').then(m => m.CartPageModule)
  // },
  // {
  //   path: ':res/booking',
  //   loadChildren: () => import('./booking/booking.module').then(m => m.BookingPageModule)
  // },
  // {
  //   path: ':res/queue',
  //   loadChildren: () => import('./queue/queue.module').then(m => m.QueuePageModule)
  // },
  // {
  //   path: ':res/queue/rawqueue/queuecart',
  //   loadChildren: () => import('./queue/rawqueue/queuecart/queuecart.module').then(m => m.QueuecartPageModule)
  // },
  // {
  //   path: ':res/booking/bookcart',
  //   loadChildren: () => import('./booking/booking.module').then(m => m.BookingPageModule)
  // },
  // {
  //   path: ':res/queue/rawqueue',
  //   loadChildren: () => import('./queue/queue.module').then(m => m.QueuePageModule)
  // },
  // {
  //   path: ':res/booking/cart',
  //   loadChildren: () => import('./booking/bookcart/confirmcart/confirmcart.module').then(m => m.ConfirmcartPageModule)
  // },
  // {
  //   path: ':res',
  //   loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  //   // loadChildren: () => import('./main/main.module').then(m => m.MainPageModule)

  // },
  // {
  //   path: 'menu-tab',
  //   loadChildren: () => import('./menu-tab/menu-tab.module').then( m => m.MenuTabPageModule)
  // },
  // {
  //   path: 'header-default',
  //   loadChildren: () => import('./header-default/header-default.module').then( m => m.HeaderDefaultPageModule)
  // }
 
  // {
  //   path: 'booking',
  //   loadChildren: () => import('./booking/booking.module').then( m => m.BookingPageModule)
  // },
  // {
  //   path: 'queue',
  //   loadChildren: () => import('./queue/queue.module').then( m => m.QueuePageModule)
  // },
  // {
  //   path: 'review',
  //   loadChildren: () => import('./review/review.module').then( m => m.ReviewPageModule)
  // },
  // {
  //   path: 'receive',
  //   loadChildren: () => import('./tab1/receive/receive.module').then( m => m.ReceivePageModule)
  // }
  

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
