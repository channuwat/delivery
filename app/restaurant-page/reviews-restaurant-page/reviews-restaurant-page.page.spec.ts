import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReviewsRestaurantPagePage } from './reviews-restaurant-page.page';

describe('ReviewsRestaurantPagePage', () => {
  let component: ReviewsRestaurantPagePage;
  let fixture: ComponentFixture<ReviewsRestaurantPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewsRestaurantPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReviewsRestaurantPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
