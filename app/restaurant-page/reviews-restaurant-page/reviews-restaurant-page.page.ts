import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reviews-restaurant-page',
  templateUrl: './reviews-restaurant-page.page.html',
  styleUrls: ['./reviews-restaurant-page.page.scss'],
})
export class ReviewsRestaurantPagePage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public color_rate = 0;
  public rate_same = true;
  public star_loop = [1,2,3,4,5]
  giveRate(rate) {
    if (rate == this.color_rate) {
      this.rate_same = true;
      this.color_rate = 0
      this.star_loop = [1,2,3,4,5]
    }
    else {
      this.color_rate = rate;
      this.rate_same = false;
      this.star_loop = [1,2,3,4,5]
    }
    console.log(this.color_rate);

  }

}
