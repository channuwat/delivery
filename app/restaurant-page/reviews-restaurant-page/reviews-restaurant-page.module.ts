import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReviewsRestaurantPagePageRoutingModule } from './reviews-restaurant-page-routing.module';

import { ReviewsRestaurantPagePage } from './reviews-restaurant-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReviewsRestaurantPagePageRoutingModule,
  ],
  declarations: [ReviewsRestaurantPagePage],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class ReviewsRestaurantPagePageModule {}
