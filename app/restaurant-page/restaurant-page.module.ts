import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RestaurantPagePageRoutingModule } from './restaurant-page-routing.module';

import { RestaurantPagePage } from './restaurant-page.page';
import { DeliveryModalPage } from '../delivery-modal/delivery-modal.page';
import { DeliveryModalPageModule } from '../delivery-modal/delivery-modal.module';
import { OrderModalPage } from './order-modal/order-modal.page';
import { OrderModalPageModule } from './order-modal/order-modal.module';
import { LoginModalPage } from '../login-regis/login-modal/login-modal.page';
import { LoginModalPageModule } from '../login-regis/login-modal/login-modal.module';
import { RegisModalPage } from '../login-regis/regis-modal/regis-modal.page';
import { RegisModalPageModule } from '../login-regis/regis-modal/regis-modal.module';
import { OtpModalPage } from '../login-regis/otp-modal/otp-modal.page';
import { OtpModalPageModule } from '../login-regis/otp-modal/otp-modal.module';
import { QueueModalPage } from '../queue-modal/queue-modal.page';
import { QueueModalPageModule } from '../queue-modal/queue-modal.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RestaurantPagePageRoutingModule,
    DeliveryModalPageModule,
    OrderModalPageModule,
    ReactiveFormsModule,
    LoginModalPageModule,
    RegisModalPageModule,
    OtpModalPageModule,
    QueueModalPageModule
  ],
  declarations: [RestaurantPagePage],
  entryComponents:[DeliveryModalPage,OrderModalPage,LoginModalPage,RegisModalPage,OtpModalPage,QueueModalPage]
})
export class RestaurantPagePageModule {}
