import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RestaurantPagePage } from './restaurant-page.page';

const routes: Routes = [
  {
    path: '',
    component: RestaurantPagePage
  },
  {
    path: 'reviews-restaurant-page',
    loadChildren: () => import('./reviews-restaurant-page/reviews-restaurant-page.module').then( m => m.ReviewsRestaurantPagePageModule)
  },
  {
    path: 'order-modal',
    loadChildren: () => import('./order-modal/order-modal.module').then( m => m.OrderModalPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RestaurantPagePageRoutingModule {}
