import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WabapiService } from '../wabapi.service';
import { Storage } from '@ionic/storage';
import { ModalController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MenuController } from '@ionic/angular';


import { DeliveryModalPage } from '../delivery-modal/delivery-modal.page';
import { OrderModalPage } from './order-modal/order-modal.page';
import { OrderMapPage } from '../delivery-modal/order-map/order-map.page';
import { OrderDetailModalPage } from '../delivery-modal/order-detail-modal/order-detail-modal.page';
import { LoginModalPage } from '../login-regis/login-modal/login-modal.page';
import { RegisModalPage } from '../login-regis/regis-modal/regis-modal.page';
import { OtpModalPage } from '../login-regis/otp-modal/otp-modal.page';
import { QueueModalPage } from '../queue-modal/queue-modal.page';


declare var FB: any;
@Component({
  selector: 'app-restaurant-page',
  templateUrl: './restaurant-page.page.html',
  styleUrls: ['./restaurant-page.page.scss'],
})
export class RestaurantPagePage implements OnInit {
  public regisForm: FormGroup
  public loginForm: FormGroup

  /**จำนวนในเเต่ละรายการ */
  public amount = [];

  public res_data = { name: '', address: '', cover_url: '' }
  public res = '';
  public res_id = 0
  public res_uri = ''
  public user = { mm_id: null, name: '' }
  public load = true
  constructor(
    public route: ActivatedRoute,
    public service: WabapiService,
    public store: Storage,
    public modal: ModalController,
    private ref: ChangeDetectorRef,
    public loadingController: LoadingController,
    private menu: MenuController
  ) {
    this.store.remove('restaurant')
    this.store.remove('store_foods')
    this.store.remove('store_group_foods')

    this.res = this.route.snapshot.paramMap.get('res');
    this.store.get('user').then((data: any) => {
      if (data != null) {
        this.user = data
        setTimeout(() => {
          this.order_load = false;
          if (this.user != null) {
            this.loadOrder(this.user.mm_id)
          }
        }, 1500);
      } else {
        this.order_load = false;
      }
    })


    this.store.get('restaurant').then(data => {
      if (data == null) {
        this.service.getData('API_emp_app3/get_res/' + this.res + '?lang=th').then((result: any) => {
          if (result.flag) {
            this.res_data = result.data;
            this.res_id = result.data.id_res_auto
            this.res_uri = result.data.uri
            this.store.set('restaurant', this.res_data).then(() => {
            })
          }
        })
      } else {
        this.res_id = data.id_res_auto
        this.res_uri = data.uri
      }
      this.ref.detectChanges()
    })
  }

  ionViewDidEnter() {
    if (this.user.mm_id == null) {
      this.store.get('user').then((data: any) => {
        if (data != null) {
          this.user = data
          setTimeout(() => {
            this.order_load = false;
            if (this.user != null) {
              this.loadOrder(this.user.mm_id)
            }
          }, 1500);
        } else {
          this.order_load = false;
        }
      })
    }
  }

  ionViewWillLeave() {
    this.load = false
  }

  ngOnInit() {
    this.loaddata();
    this.service.fb_value('data_pay', () => {
      if (this.load) {
        if (this.user != null) {
          this.loadOrder(this.user.mm_id)
        }
      }

    })
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }


  public order_history = []
  public order_history_cc = []
  public order_load = true;
  loadOrder(user_id) {
    this.service.getData('Delivery_order/getOrder/' + user_id + '/' + this.res_id).then((order: any) => {
      if (order != null) {
        this.order_history = order.order_active
        this.order_history_cc = order.order_cancel
        this.order_load = false;

        this.order_history.forEach(element => {
          if (element.status == 0) {
            element['status_txt'] = 'ยกเลิก'
          } else if (element.status == 1) {
            element['status_txt'] = 'ส่งออเดอร์ให้ร้านค้า'
          } else if (element.status == 2) {
            element['status_txt'] = 'ร้านค้ารับออเดอร์'
          } else if (element.status == 3) {
            element['status_txt'] = 'กำลังทำออเดอร์'
          } else if (element.status == 4) {
            element['status_txt'] = 'กำลังออกส่ง'
          }
        });
        this.ref.detectChanges()
      } else {
        this.order_history = []
        this.order_history_cc = []
        this.ref.detectChanges()
      }
    })
  }

  public login = { status: true, count: 0 }
  public register = { status: true, count: 0 }
  signIn(type) {
    if (type == 'login') {
      if (this.login.count == 0) {
        this.login.status = false
        this.register.status = true
        this.login.count++
      } else {
        this.login.status = true
        this.login.count--
      }
      this.register.count = 0
    } else {
      if (this.register.count == 0) {
        this.login.status = true
        this.register.status = false
        this.register.count++
      } else {
        this.register.status = true
        this.register.count--
      }
      this.login.count = 0
    }
  }

  // closeSignIn() {
  //   this.login = { status: true, count: 0 }
  //   this.register = { status: true, count: 0 }
  // }


  logoutUser() {
    this.store.remove('user')
    // this.user = { mm_id: 0, name: '' }
    // this.loadOrder(this.user.mm_id)
    setTimeout(() => {
      window.location.reload();
    }, 500);
  }

  loaddata() {
    this.presentLoading()
    this.service.getData('API_emp_app3/get_res/' + this.res + '?lang=th').then((result: any) => {
      if (result.flag) {
        this.res_data = result.data;
        this.store.set('restaurant', this.res_data) // set storage restaurant default
        this.loadProduct(result.data.id_res_auto)

      } else {
        setTimeout(() => {
          this.loadingController.dismiss()
        }, 1500)

      }
    })
  }

  loadProduct(res_id) {
    this.store.get('store_group_foods').then((group_data: any) => {
      this.service.getData('API_emp_app3/get_all_product/' + res_id).then((foods: any) => {
        this.store.set('store_group_foods', foods.group)
        this.store.set('store_foods', foods.foods)
        this.loadingController.dismiss()
      })
    })
  }

  addItem(i_main) {
    this.amount[i_main]++
  }

  deleteItem(i_main) {
    this.amount[i_main]--
    if (this.amount[i_main] < 0) {
      this.amount[i_main] = 0
    }
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'กำลังโหลดข้อมูลร้านค้า...',
    });
    await loading.present();
  }

  async signInModal(data, condition) {
    let modal_type: any
    let css_modal: string
    if (condition == 'login') {
      modal_type = LoginModalPage
      css_modal = 'login_modal'
    } else if (condition == 'regis') {
      modal_type = RegisModalPage
      css_modal = 'regis_modal'
    } else if (condition == 'otp') {
      modal_type = OtpModalPage
      css_modal = 'otp_modal'
    }

    const modal = await this.modal.create({
      component: modal_type,
      componentProps: { data: data },
      backdropDismiss: false,
      cssClass: css_modal
    });

    modal.onWillDismiss().then((event: any) => {
      let data_obj = event.data
      event = event.role
      if (event == 'exit') {

      } else if (event == 'เข้าสู่ระบบสำเร็จ') {
        this.store.get('user').then((data: any) => {
          if (data != null) {
            this.menu.close()
            this.user = data
            this.loadOrder(this.user.mm_id)
            this.openDeliveryModal(this.res_id)
            setTimeout(() => {
              this.ref.detectChanges()
            }, 1000);
          }
        })
      } else if (event == 'ลืมรหัสผ่าน') {

      } else if (event == 'สมัครสมาชิก') {
        this.signInModal(data_obj, 'regis')
      }
      else if (event == 'ขอotp') {
        this.signInModal(data_obj, 'otp')
      }
    })

    return await modal.present();
  }

  async showOrder(order_id) {
    const modal = await this.modal.create({
      component: OrderModalPage,
      componentProps: {
        'order_id': order_id,
        'res_id': this.res_id
      },
      //backdropDismiss: false

    });

    modal.onWillDismiss().then((event: any) => {
      event = event.data
      if (event = 'สำเร็จ') {
        this.loadOrder(this.user.mm_id)
      }
    })

    return await modal.present();
  }


  async openDeliveryModal(res_id) {
    const modal = await this.modal.create({
      component: DeliveryModalPage,
      cssClass: 'dalivery_modal',
      componentProps: {
        Res_id: res_id
      }
    });

    modal.onWillDismiss().then((error_txt) => {
      error_txt = error_txt.data
      if (error_txt == 'exit_final') {
      } else if (error_txt == 'open_map') {
        this.openMap()
      } else if (error_txt == 'login') {
        this.store.get('user').then((data: any) => {
          if (data != null) {
            this.service.getData('Delivery_order/checkPermissionOrder/' + data.mm_id).then((data_u: any) => {
              if (data_u == 'คุณมีออเดอร์ค้างชำระ') {
                this.service.Toast(data_u)
              } else {
                this.openMap()
              }
            })
          } else {
            this.service.Toast('โปรดลงชื่อเข้าใช้ระบบก่อน')
            this.signInModal({}, 'login')
          }
        })

      }
    })

    return await modal.present();
  }

  async openMap() {
    this.modal.dismiss()
    const modal = await this.modal.create({
      component: OrderMapPage,
      backdropDismiss: false
    });
    modal.onDidDismiss().then((data: any) => {
      let error_txt = data.data.txt_condition
      let route_data = data.data.route
      if (error_txt == 'order_detail_modal') {
        this.showDeriveryDetail(route_data)
      } else if (error_txt == 'order_item_list') {
        this.openDeliveryModal(this.res_id)
      }
    });
    modal.present();
  }

  async showDeriveryDetail(route_data) {
    this.modal.dismiss()
    const modal = await this.modal.create({
      component: OrderDetailModalPage,
      componentProps: { route_data: route_data },
      backdropDismiss: false,
    });


    modal.onDidDismiss().then((data: any) => {
      if (data.data == 'add_success') {
        this.service.Toast('สั่งออเดอร์สำเร็จ √')
        this.ref.detectChanges()
        setTimeout(() => {
          this.showOrder(this.order_history[0].order_id)
        }, 1000);

      } else if (data.data == 'open_map') {
        this.openMap()
      } else if ('open_item_list') {
        this.openDeliveryModal(this.res_id)
      }
    });

    await modal.present();

  }

  async openQueueModal() {
    const modal = await this.modal.create({
      component: QueueModalPage,
      cssClass: 'queue_modal',
    });

    modal.onWillDismiss().then((error_txt) => {

    })

    return await modal.present();
  }

}
