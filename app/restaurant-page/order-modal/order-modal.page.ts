import { Component, OnInit, Input, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { WabapiService } from 'src/app/wabapi.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
declare var google;

@Component({
   selector: 'app-order-modal',
   templateUrl: './order-modal.page.html',
   styleUrls: ['./order-modal.page.scss'],
})
export class OrderModalPage implements OnInit {

   public load = true
   public order_key = null
   public res_id = null
   @Input() order_id: any
   constructor(
      public modalCtrl: ModalController,
      public store: Storage,
      public service: WabapiService,
      public alertController: AlertController,
      public navParams: NavParams,
      public api: WabapiService,
      private ref_detect: ChangeDetectorRef,
      public http: HttpClient,
      private geolocation: Geolocation,
   ) {
      this.order_key = this.navParams.get('order_id')
      this.res_id = this.navParams.get('res_id')
   }


   ngOnInit() {
      this.service.fb_value('data_pay', (val) => {
         if (this.load) {
            this.setConfirmOrder(this.order_key, this.res_id)
         }
      })
   }


   ionViewWillEnter() {
   }

   ionViewWillLeave() {
      this.load = false
   }


   @ViewChild('mapElement', { static: false }) mapNativeElement: ElementRef;
   public latitude: any;
   public longitude: any;
   loadMap(lat, long, order_key, timeline) {
      var map = new google.maps.Map(this.mapNativeElement.nativeElement, {
         zoom: 14.8,
         center: new google.maps.LatLng(parseFloat(lat), parseFloat(long)),
         mapTypeId: google.maps.MapTypeId.ROADMAP
      });

      var marker

      if (timeline == 4) {

         var marker2 = new google.maps.Marker({
            position: new google.maps.LatLng(lat, long),
            map: map,
            icon: {
               url: 'https://img.icons8.com/plasticine/72/order-shipped.png',
               scaledSize: new google.maps.Size(50, 50),
            }
         });
         this.service.fb_value("rider/" + order_key, (val) => {
            if (this.load) {
               marker2.setPosition(new google.maps.LatLng(val.val().lat, val.val().lng));
            }
         });
      }
      marker = new google.maps.Marker({
         position: new google.maps.LatLng(lat, long),
         map: map,
         icon: {
            url: 'assets/icon/icon_map/u.png',
            scaledSize: new google.maps.Size(50, 50),
         }
      });
   }

   public data_details = {
      detail: [],
      order:
      {
         address: "",
         member_lat_long: { lat: 0, long: 0 },
         member_comment: "",
         member_giveaway: "0",
      },
      slip: [{ img_url: '' }],
      timeline: 0
   }
   public total = 0
   public bank = []
   setConfirmOrder(id_order, res_id) {
      this.api.getData('Delivery_order/getDetailOrder/' + id_order + '/' + res_id).then((detail: any) => {
         this.data_details = detail

         // set timeline //
         this.timeline_st = detail.timeline
         let lat = this.data_details.order.member_lat_long.lat
         let long = this.data_details.order.member_lat_long.long
         setTimeout(() => {
            this.loadMap(lat, long, id_order, this.data_details.timeline);
            setTimeout(() => {
               this.ref_detect.detectChanges()
            }, 1000);
         }, 500);

         // set ประเภทการจ่ายเงิน
         if (this.data_details.slip.length == 0) {
            this.data_details.slip = [{ img_url: '' }]
         }
         this.bank = detail.bank
         this.id_bank = detail.order.payment_type
         if (detail.slip != '') {
            this.url = detail.slip[0].img_url
         }
         this.type_pay = detail.order.payment_type
         if (this.type_pay > 0) {
            this.pay_status = 1
         }

         // set รายการอารหาร //
         this.store.get('store_foods').then(data_food => {
            data_food.forEach(item_food => {
               this.data_details.detail.forEach(item_detail => {
                  if (item_food.f_id == item_detail.f_id) {
                     item_detail['name'] = item_food.name
                     this.total += parseInt(item_detail.total_price)
                  }
               });
            });
            this.total = this.total + parseInt(detail.order.price_send)
         })
      })
   }

   get_html(c): String {
      let out: String = '';
      for (let entry of c.details) {
         // let price = 0;
         let temp = '';
         let flag = true;
         for (let sub of entry.sub) {
            if (sub.selected) {
               if (flag) {
                  flag = false;
                  temp += sub.name[0].title;
               } else {
                  temp += ", " + sub.name[0].title;
               }
            }
         }
         if (temp != '') {
            out += entry.name[0].title + " : ";
            out += temp;
            out += "<br>";
         }

      }

      for (let entry of c.toppings) {
         if (entry.count - 0 > 0) {
            out += "  [" + entry.count + "] " + entry.title + " (";
            if (entry.price > 0) {
               out += "+";
            }
            out += (entry.price * entry.count) + ")";
            out += "<br>";
         }
      }
      // if (c.comments.trim() != '') {
      //    out += "Comment : " + c.comments;
      // }

      return out;
   }

   public timeline_st: number = 0
   public state_txt = [
      'ส่งออรเดอร์ให้ร้านค้า',
      'ร้านค้ารับออเดอร์',
      'กำลังทำออเดอร์',
      'กำลังออกส่ง'
   ]

   public showDL = true
   showDetaildelivery(status) {
      if (status) {
         this.showDL = false
      } else {
         this.showDL = true
      }
   }

   public type_pay = 0
   public pay_status = 0
   chkTypePay(type) {
      this.pay_status = type
   }

   public imagePath: any = '';
   public url = '';
   public cf_slip = 0
   async add_slip(event) {
      event.preventDefault();
      let element: HTMLElement = document.getElementById('slipinput') as HTMLElement;
      element.click();
   }
   changeListener(event): void {
      if (event.target.files[0].name != '') {
         this.cf_slip = 1
      } else {
         this.cf_slip = 0
      }

      const formData = new FormData();
      formData.append('avatar', event.target.files[0]);

      this.imagePath = formData
      var reader = new FileReader();
      reader.onload = (event: any) => {
         this.url = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
   }

   async confirmSlip(order_id) {
      const alert = await this.alertController.create({
         header: 'ยืนยันสลิปการโอนเงิน?',
         buttons: [
            {
               text: 'ยกเลิก',
               role: 'cancel',
               cssClass: 'secondary',
               handler: (count) => {
                  this.service.Toast('cancel')
               }
            }, {
               text: 'ยืนยัน',
               handler: () => {
                  this.service.Toast('updated')
                  this.http.post(this.api.baseUrlApi + '../Delivery_order/uploadfile', this.imagePath).subscribe((res: any) => {
                     let data = {
                        order_id: this.order_key,
                        id_bank: this.id_bank,
                        img: res.url,
                        res_id: this.res_id
                     }
                     this.service.postData('store/updateSlip', data).then(() => {
                        this.service.fb_set('data_pay')
                     })
                  });
               }
            }
         ]
      });

      await alert.present();

   }

   async presentAlertConfirm(order_id, status) {
      const alert = await this.alertController.create({
         header: 'ยกเลิกออเดอร์?',
         buttons: [
            {
               text: 'ยกเลิก',
               role: 'cancel',
               cssClass: 'secondary',
               handler: (count) => {
                  // null
               }
            }, {
               text: 'ตกลง',
               handler: () => {
                  // some code update API 
                  this.api.postData('Delivery_order/updateConfirmOrder', { order_id: order_id, status: status }).then((data: any) => {
                     if (data == 'สำเร็จ') {
                        this.close('สำเร็จ')
                     } else {
                        this.service.Toast('ยืนยันคำสั่งซื้อเเล้วไม่สามารถยกเลิกได้')
                     }
                  })

               }
            }
         ]
      });
      await alert.present();
   }

   public id_bank: number = 0
   public copyMessage(val: string, bank_id: number) {
      const selBox = document.createElement('textarea');
      selBox.style.position = 'fixed';
      selBox.style.left = '0';
      selBox.style.top = '0';
      selBox.style.opacity = '0';
      selBox.value = val;
      document.body.appendChild(selBox);
      selBox.focus();
      selBox.select();
      document.execCommand('copy');
      document.body.removeChild(selBox);
      this.service.Toast('คัดลอกเลขบัญชีธนาคาร ' + val)
      this.id_bank = bank_id
   }

   confirmOrder(cf, cart_data, total) {
      if (cf) {
      } else {
      }
      this.close('exit')
   }

   close(event) {
      this.modalCtrl.dismiss(event)
   }

}
