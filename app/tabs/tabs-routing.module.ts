import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: '',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../home/home.module').then(m => m.HomePageModule)
          }
        ]
      },
      {
        path: 'notifications',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../home/home.module').then(m => m.HomePageModule)
          }
        ]
      },
      {
        path: 'all-order',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../all-order-page/all-order-page.module').then(m => m.AllOrderPagePageModule)
          }
        ]
      },
      {
        path: 'config',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../home-config-page/home-config-page.module').then(m => m.HomeConfigPagePageModule)
          }
        ]
      }
      // {
      //   path: 'history',
      //   children: [
      //     {
      //       path: '',
      //       loadChildren: () =>
      //         import('../tab2/tab2.module').then(m => m.Tab2PageModule)
      //     }
      //   ]
      // },
      // {
      //   path: 'settings',
      //   children: [
      //     {
      //       path: '',
      //       loadChildren: () =>
      //         import('../tab3/tab3.module').then(m => m.Tab3PageModule)
      //     }
      //   ]
      // },
      // {
      //   path: '',
      //   redirectTo: '/khemcs',
      //   pathMatch: 'full'
      // }
    ]
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
