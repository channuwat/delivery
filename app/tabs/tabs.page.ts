import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// import { ActivatedRouteSnapshot } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  public res = '';
  public type = '';
  constructor(private route: ActivatedRoute,
    private router: Router) {
    this.res = this.route.snapshot.paramMap.get('res');
    this.type = this.route.snapshot.paramMap.get('type');
    if (this.type == 'dalivery') {
      this.router.navigateByUrl('/' + this.res + '/delivery');
    }

  }
  click_tab(uri) {

      this.router.navigateByUrl('/' + uri);
    

  }

}
