import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeConfigPagePage } from './home-config-page.page';

const routes: Routes = [
  {
    path: '',
    component: HomeConfigPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeConfigPagePageRoutingModule {}
