import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeConfigPagePageRoutingModule } from './home-config-page-routing.module';

import { HomeConfigPagePage } from './home-config-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeConfigPagePageRoutingModule
  ],
  declarations: [HomeConfigPagePage]
})
export class HomeConfigPagePageModule {}
