import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AllOrderPagePageRoutingModule } from './all-order-page-routing.module';

import { AllOrderPagePage } from './all-order-page.page';
import { AllOrderDetailPagePage } from './all-order-detail-page/all-order-detail-page.page';
import { AllOrderDetailPagePageModule } from './all-order-detail-page/all-order-detail-page.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AllOrderPagePageRoutingModule,
    AllOrderDetailPagePageModule
  ],
  declarations: [AllOrderPagePage],
  entryComponents:[AllOrderDetailPagePage]
})
export class AllOrderPagePageModule {}
