import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AllOrderDetailPagePage } from './all-order-detail-page/all-order-detail-page.page';

@Component({
  selector: 'app-all-order-page',
  templateUrl: './all-order-page.page.html',
  styleUrls: ['./all-order-page.page.scss'],
})
export class AllOrderPagePage implements OnInit {

  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }

  async modalOrder(order_id) {
    const modal = await this.modalController.create({
      component: AllOrderDetailPagePage
    });
    return await modal.present();
  }
}
