import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllOrderPagePage } from './all-order-page.page';

const routes: Routes = [
  {
    path: '',
    component: AllOrderPagePage
  },
  {
    path: 'all-order-detail-page',
    loadChildren: () => import('./all-order-detail-page/all-order-detail-page.module').then( m => m.AllOrderDetailPagePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllOrderPagePageRoutingModule {}
