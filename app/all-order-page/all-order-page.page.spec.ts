import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AllOrderPagePage } from './all-order-page.page';

describe('AllOrderPagePage', () => {
  let component: AllOrderPagePage;
  let fixture: ComponentFixture<AllOrderPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllOrderPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AllOrderPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
