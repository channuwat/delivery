import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllOrderDetailPagePage } from './all-order-detail-page.page';

const routes: Routes = [
  {
    path: '',
    component: AllOrderDetailPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllOrderDetailPagePageRoutingModule {}
