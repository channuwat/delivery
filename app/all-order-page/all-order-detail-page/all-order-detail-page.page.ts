import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-all-order-detail-page',
  templateUrl: './all-order-detail-page.page.html',
  styleUrls: ['./all-order-detail-page.page.scss'],
})
export class AllOrderDetailPagePage implements OnInit {

  public order_li = [1, 2, 3, 4, 5]
  public amout = []
  constructor(public modalCtrl: ModalController) {
  }

  
  ngOnInit() {
    this.order_li.forEach((element, index) => {
      this.amout[index] = 0
    });
  }

  close() {
    this.modalCtrl.dismiss()
  }

  additem(add, index) {
    console.log(add,index);
    
    this.amout[index] = this.amout[index] + add
    if (this.amout[index] < 0) {
      this.amout[index] = 0
    }
  }

}
