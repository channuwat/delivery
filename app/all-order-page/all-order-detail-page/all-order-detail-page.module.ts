import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AllOrderDetailPagePageRoutingModule } from './all-order-detail-page-routing.module';

import { AllOrderDetailPagePage } from './all-order-detail-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AllOrderDetailPagePageRoutingModule
  ],
  declarations: [AllOrderDetailPagePage]
})
export class AllOrderDetailPagePageModule {}
