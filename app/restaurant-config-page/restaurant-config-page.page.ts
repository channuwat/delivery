import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { WabapiService } from '../wabapi.service';
import { Storage } from '@ionic/storage';
import { ModalController, LoadingController } from '@ionic/angular';
import { LoginModalPage } from '../login-regis/login-modal/login-modal.page';
import { RegisModalPage } from '../login-regis/regis-modal/regis-modal.page';
import { OtpModalPage } from '../login-regis/otp-modal/otp-modal.page';

@Component({
  selector: 'app-restaurant-config-page',
  templateUrl: './restaurant-config-page.page.html',
  styleUrls: ['./restaurant-config-page.page.scss'],
})
export class RestaurantConfigPagePage implements OnInit {
  public route_res = ''
  constructor(
    private router: Router,
    public service: WabapiService,
    public store: Storage,
    public modal: ModalController,
    public loadingController: LoadingController,
    private ref: ChangeDetectorRef,
  ) {

    
  }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.auth()
    this.store.get('restaurant').then((res: any) => {
      this.route_res = res.uri
    })
    
  }

  clickLink(sub_menu) {
    this.router.navigateByUrl('deltafood/config/' + sub_menu);
  }

  logoutUser() {
    this.store.remove('user')
    setTimeout(() => {
      this.router.navigateByUrl('/' + this.route_res);
      window.location.reload();
    }, 500);
  }

  public auth_u : any = null
  auth(){
    this.store.get('user').then((user_data:any)=>{
      if(user_data != null){
        this.auth_u = user_data
      }
      
    })
  }

  async signInModal(data, condition) {
    let modal_type: any
    let css_modal: string
    if (condition == 'login') {
      modal_type = LoginModalPage
      css_modal = 'login_modal'
    } else if (condition == 'regis') {
      modal_type = RegisModalPage
      css_modal = 'regis_modal'
    } else if (condition == 'otp') {
      modal_type = OtpModalPage
      css_modal = 'otp_modal'
    }

    const modal = await this.modal.create({
      component: modal_type,
      componentProps: { data: data },
      backdropDismiss: false,
      cssClass: css_modal,
    });

    modal.onWillDismiss().then((event: any) => {
      let data_obj = event.data
      event = event.role
      if (event == 'exit') {

      } else if (event == 'เข้าสู่ระบบสำเร็จ') {
        this.auth()
        this.router.navigateByUrl('/' + this.route_res);
        
      } else if (event == 'ลืมรหัสผ่าน') {

      } else if (event == 'สมัครสมาชิก') {
        this.signInModal(data_obj, 'regis')
      }
      else if (event == 'ขอotp') {
        this.signInModal(data_obj, 'otp')
      }
    })

    return await modal.present();
  }

}
