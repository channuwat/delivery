import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RestaurantConfigPagePage } from './restaurant-config-page.page';

const routes: Routes = [
  {
    path: '',
    component: RestaurantConfigPagePage,
  },
  {
    path: 'address',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('../restaurant-config-page/config-address/config-address.module').then(m => m.ConfigAddressPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RestaurantConfigPagePageRoutingModule {}
