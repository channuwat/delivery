import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-config-address',
  templateUrl: './config-address.page.html',
  styleUrls: ['./config-address.page.scss'],
})
export class ConfigAddressPage implements OnInit {

  constructor(public router : Router) { }

  ngOnInit() {
  }

  linkAdress(uri){
    console.log(uri);
    
    this.router.navigateByUrl('deltafood/config/address/'+uri);
  }
}
