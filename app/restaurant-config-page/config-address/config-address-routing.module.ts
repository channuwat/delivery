import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfigAddressPage } from './config-address.page';

const routes: Routes = [
  {
    path: '',
    component: ConfigAddressPage
  },
  {
    path: 'add_address',
    loadChildren: () => import('./add-address/add-address.module').then( m => m.AddAddressPageModule),
  },
  {
    path: 'detail_address',
    loadChildren: () => import('./detail-address/detail-address.module').then( m => m.DetailAddressPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigAddressPageRoutingModule {}
