import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfigAddressPageRoutingModule } from './config-address-routing.module';

import { ConfigAddressPage } from './config-address.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfigAddressPageRoutingModule
  ],
  declarations: [ConfigAddressPage]
})
export class ConfigAddressPageModule {}
