import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConfigAddressPage } from './config-address.page';

describe('ConfigAddressPage', () => {
  let component: ConfigAddressPage;
  let fixture: ComponentFixture<ConfigAddressPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigAddressPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConfigAddressPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
