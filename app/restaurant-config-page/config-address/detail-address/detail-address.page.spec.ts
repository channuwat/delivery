import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailAddressPage } from './detail-address.page';

describe('DetailAddressPage', () => {
  let component: DetailAddressPage;
  let fixture: ComponentFixture<DetailAddressPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailAddressPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailAddressPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
