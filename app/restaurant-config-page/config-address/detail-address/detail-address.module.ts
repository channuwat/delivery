import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailAddressPageRoutingModule } from './detail-address-routing.module';

import { DetailAddressPage } from './detail-address.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailAddressPageRoutingModule
  ],
  declarations: [DetailAddressPage]
})
export class DetailAddressPageModule {}
