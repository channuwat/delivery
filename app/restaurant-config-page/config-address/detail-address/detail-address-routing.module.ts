import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailAddressPage } from './detail-address.page';

const routes: Routes = [
  {
    path: '',
    component: DetailAddressPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailAddressPageRoutingModule {}
