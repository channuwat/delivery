import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RestaurantConfigPagePageRoutingModule } from './restaurant-config-page-routing.module';

import { RestaurantConfigPagePage } from './restaurant-config-page.page';
import { LoginModalPageModule } from '../login-regis/login-modal/login-modal.module';
import { RegisModalPageModule } from '../login-regis/regis-modal/regis-modal.module';
import { OtpModalPageModule } from '../login-regis/otp-modal/otp-modal.module';
import { LoginModalPage } from '../login-regis/login-modal/login-modal.page';
import { RegisModalPage } from '../login-regis/regis-modal/regis-modal.page';
import { OtpModalPage } from '../login-regis/otp-modal/otp-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RestaurantConfigPagePageRoutingModule,
    LoginModalPageModule,
    RegisModalPageModule,
    OtpModalPageModule,
  ],
  declarations: [RestaurantConfigPagePage],
  entryComponents : [
    LoginModalPage,
    RegisModalPage,
    OtpModalPage
  ]
})
export class RestaurantConfigPagePageModule {}
