import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.page.html',
  styleUrls: ['./booking.page.scss'],
})
export class BookingPage implements OnInit {

  public res = '';
  public type = '';
  public show = true;
  public databook = {b_name:'', b_nickname:'', b_phone:'', b_number:0, b_date:'', b_time:'', b_detail:''}
  public text1:string;


  key: string = 'username';
  inputext: string;


  constructor(
    private route: ActivatedRoute, 
    public navCtrl: NavController,
    private router: Router,
    private storage: Storage) {

    this.res = this.route.snapshot.paramMap.get('res');
    this.type = this.route.snapshot.paramMap.get('type');


  }


  ngOnInit() {
  }
  back() {
    // this.router.navigateByUrl('/' + this.res);  
    this.navCtrl.navigateRoot('/' + this.res);
  }
  
  // bookcart() {
  //   this.storage.set(this.text1,this.databook);
  //   console.log(this.databook);
  //   // this.router.navigateByUrl('/' + this.res+"/delivery/cart");
  //   this.navCtrl.navigateForward('/' + this.res+"/booking/bookcart");
    
  // }


  saveUser() {
    // set a key/value
       this.storage.set('book',this.databook);
       console.log(this.databook);
       this.navCtrl.navigateForward('/' + this.res+"/booking/bookcart");
    }

  
}
