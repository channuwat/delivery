import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, AlertController, LoadingController, ModalController } from '@ionic/angular';
import { WabapiService } from 'src/app/wabapi.service';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { BeditmodaldetailPage } from '../beditmodaldetail/beditmodaldetail.page';

@Component({
  selector: 'app-bcart',
  templateUrl: './bcart.page.html',
  styleUrls: ['./bcart.page.scss'],
})
export class BcartPage implements OnInit {

  public res = "";
  public type = '';
  public cart = [];
  public currency = 'บาท';
  public lang = "TH";
  public cart_count = 0;
  public dataprice = {sum_all:''};
  constructor(
    private route: ActivatedRoute,
    public navCtrl: NavController,
    private router: Router,
    public service: WabapiService,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    public s: Storage,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public modalController: ModalController

  ) {
    this.res = route.snapshot.paramMap.get('res');
    let type = route.snapshot.paramMap.get('type');

    this.s.get('cart').then((v) => {
      this.cart = v;
      console.log(this.cart);
    });
  

  }

  ngOnInit() {
  }
  back() {
    // this.router.navigateByUrl("/" + this.res + "/delivery");
    this.navCtrl.navigateBack("/" + this.res + "/booking/bookcart");
  }
  checkorder() {
        this.storage.set('sum',this.dataprice); 
    // this.router.navigateByUrl('/' + this.res+"/delivery/cart");
    this.navCtrl.navigateForward('/' + this.res + "/booking/bookcart/bcart/checkorder");

  }

  async Modaleditdetail(c){
    
    const modal = await this. modalController.create({
      component: BeditmodaldetailPage,
      cssClass: 'my-custom-modal-css2',
      componentProps: {data: c}
      
     
    });
    await modal.present();
    // console.log(f);
  }
  remove(index) {
    this.cart.splice(index, 1);
    this.s.get('cart').then((val) => {
      this.s.set('cart', this.cart);
    });
  }

  array_lang(text) {
    for (let val of text) {
      if (val.shot) {
        if (val.shot.toUpperCase() === this.lang) {
          return val.title;
        }
      }
    }
    return text[0].title;
  }
  get_price(c): number {
    let out: number = parseFloat(c.price);
    for (let entry of c.details) {
      for (let sub of entry.sub) {
        if (sub.selected) {
          out += parseFloat(sub.price);
        }
      }
    }
    for (let entry of c.toppings) {
      if (Number(entry.count) > 0) {
        out += parseFloat(entry.price) * Number(entry.count);
      }
    }
    return out * Number(c.count);
  }
  sum_all(){
  
    let sum = 0;
    this.cart.forEach(element => {
      sum += this.get_price(element);
 });
 return sum;
  }
  get_html(c): String {
    let out: String = '';
    for (let entry of c.details) {
      let price = 0;
      out += this.array_lang(entry.name) + ":";
      let flag = true;
      for (let sub of entry.sub) {
        if (sub.selected) {
          if (flag) {
            flag = false;
            out += this.array_lang(sub.name);
          } else {
            out += ", " + this.array_lang(sub.name);
          }
          price += sub.price - 0;
        }
      }
      if (price - 0 > 0) {
        out += " (+" + price + ")";
      } else if (price - 0 < 0) {
        out += " (" + price + ")";
      }
      out += "<br>";
    }

    for (let entry of c.toppings) {
      if (entry.count - 0 > 0) {
        out += "[" + entry.count + "] " + entry.title;
        out += " (";
        if (entry.price != 0) {
          if (entry.price - 0 > 0) {
            out += '+';
          }
          out += entry.price * entry.count + " บาท)<br>";
        } else {
          this.translate.get("free").subscribe(free => {
            out += free + ")<br>";
          });

        }
      }
    }
    out += c.comments;
    return out;
  }
}
