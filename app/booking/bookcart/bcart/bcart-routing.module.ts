import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BcartPage } from './bcart.page';

const routes: Routes = [
  {
    path: '',
    component: BcartPage
  },
  {
    path: 'checkorder',
    loadChildren: () => import('./checkorder/checkorder.module').then( m => m.CheckorderPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BcartPageRoutingModule {}
