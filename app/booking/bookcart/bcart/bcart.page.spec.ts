import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BcartPage } from './bcart.page';

describe('BcartPage', () => {
  let component: BcartPage;
  let fixture: ComponentFixture<BcartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BcartPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BcartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
