import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BcartPageRoutingModule } from './bcart-routing.module';

import { BcartPage } from './bcart.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BcartPageRoutingModule
  ],
  declarations: [BcartPage]
})
export class BcartPageModule {}
