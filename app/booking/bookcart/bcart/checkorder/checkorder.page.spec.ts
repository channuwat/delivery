import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckorderPage } from './checkorder.page';

describe('CheckorderPage', () => {
  let component: CheckorderPage;
  let fixture: ComponentFixture<CheckorderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckorderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckorderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
