import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-checkorder',
  templateUrl: './checkorder.page.html',
  styleUrls: ['./checkorder.page.scss'],
})
export class CheckorderPage implements OnInit {
  public res = "";
  public type = '';
  public sum = '';
  public cart = [];
  public d = { id_user:2, id_res:2, foods: [] , sum_all:0};
  constructor(
    public navCtrl: NavController,
    private route: ActivatedRoute,
    private storage: Storage,
    private http: HttpClient
  ) {
    this.res = route.snapshot.paramMap.get('res');
    let type = route.snapshot.paramMap.get('type');

    this.storage.get('book').then(val => {
      this.d = val;
      console.log('You name is', this.d);
    });
    this.storage.get('sum').then(price => {
      this.sum = price;
      console.log('price : ', this.sum);
    });

    this.storage.get('cart').then((v) => {
      this.cart = v;
      console.log(this.cart);
    });
  }

  ngOnInit() {
  }

  back() {
    // this.router.navigateByUrl('/' + this.res+"/delivery/cart");
    this.navCtrl.navigateForward('/' + this.res + "/booking/bookcart/bcart");

  }


  save_order() {
    const bdata = new FormData();
    this.storage.get('cart').then((v) => {
      console.log(this.cart);
      this.d.foods = v;
      bdata.append('book', JSON.stringify(this.d));
      //bdata.append('cart',JSON.stringify(this.cart));
      console.log('databookdddd :', this.d);
      //console.log('cart : ',this.cart);

      // fdata.append('image', this.fileData, this.fileData.name);
      this.http.post('https://api.deltafood.co/reviews/order', bdata, {

      })
        .subscribe((events: any) => {
          console.log('testevent', events);
        })
    });
  }
}
