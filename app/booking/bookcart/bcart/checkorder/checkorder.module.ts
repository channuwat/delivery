import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckorderPageRoutingModule } from './checkorder-routing.module';

import { CheckorderPage } from './checkorder.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckorderPageRoutingModule
  ],
  declarations: [CheckorderPage]
})
export class CheckorderPageModule {}
