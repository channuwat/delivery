import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckorderPage } from './checkorder.page';

const routes: Routes = [
  {
    path: '',
    component: CheckorderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckorderPageRoutingModule {}
