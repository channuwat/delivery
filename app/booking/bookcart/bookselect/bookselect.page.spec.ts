import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookselectPage } from './bookselect.page';

describe('BookselectPage', () => {
  let component: BookselectPage;
  let fixture: ComponentFixture<BookselectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookselectPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookselectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
