import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { WabapiService } from 'src/app/wabapi.service';

@Component({
  selector: 'app-bookselect',
  templateUrl: './bookselect.page.html',
  styleUrls: ['./bookselect.page.scss'],
})
export class BookselectPage implements OnInit {

  public res = '';
  public type = '';

  constructor(private route: ActivatedRoute, public navCtrl: NavController, private router: Router,public service: WabapiService) { 
    this.res = this.route.snapshot.paramMap.get('res');
    this.type = this.route.snapshot.paramMap.get('type');
  }

  ngOnInit() {
  }
  back() {
    // this.router.navigateByUrl("/" + this.res + "/delivery");
    this.navCtrl.navigateBack("/" + this.res + "/booking/bookcart");
  }

}
