import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookselectPageRoutingModule } from './bookselect-routing.module';

import { BookselectPage } from './bookselect.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookselectPageRoutingModule
  ],
  declarations: [BookselectPage]
})
export class BookselectPageModule {}
