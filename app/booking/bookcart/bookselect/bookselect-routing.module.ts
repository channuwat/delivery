import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookselectPage } from './bookselect.page';

const routes: Routes = [
  {
    path: '',
    component: BookselectPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookselectPageRoutingModule {}
