import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BmodaldetailPage } from './bmodaldetail.page';

describe('BmodaldetailPage', () => {
  let component: BmodaldetailPage;
  let fixture: ComponentFixture<BmodaldetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BmodaldetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BmodaldetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
