import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BmodaldetailPageRoutingModule } from './bmodaldetail-routing.module';

import { BmodaldetailPage } from './bmodaldetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BmodaldetailPageRoutingModule
  ],
  declarations: [BmodaldetailPage]
})
export class BmodaldetailPageModule {}
