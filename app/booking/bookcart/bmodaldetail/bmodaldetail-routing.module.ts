import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BmodaldetailPage } from './bmodaldetail.page';

const routes: Routes = [
  {
    path: '',
    component: BmodaldetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BmodaldetailPageRoutingModule {}
