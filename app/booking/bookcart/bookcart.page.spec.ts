import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookcartPage } from './bookcart.page';

describe('BookcartPage', () => {
  let component: BookcartPage;
  let fixture: ComponentFixture<BookcartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookcartPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookcartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
