import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookcartPageRoutingModule } from './bookcart-routing.module';

import { BookcartPage } from './bookcart.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookcartPageRoutingModule
  ],
  declarations: [BookcartPage]
})
export class BookcartPageModule {}
