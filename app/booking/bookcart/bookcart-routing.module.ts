import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookcartPage } from './bookcart.page';

const routes: Routes = [
  {
    path: '',
    component: BookcartPage
  },
  {
    path: 'confirmcart',
    loadChildren: () => import('./confirmcart/confirmcart.module').then( m => m.ConfirmcartPageModule)
  },
  {
    path: 'bookselect',
    loadChildren: () => import('./bookselect/bookselect.module').then( m => m.BookselectPageModule)
  },
  {
    path: 'bmodaldetail',
    loadChildren: () => import('./bmodaldetail/bmodaldetail.module').then( m => m.BmodaldetailPageModule)
  },
  {
    path: 'bcart',
    loadChildren: () => import('./bcart/bcart.module').then( m => m.BcartPageModule)
  },
  {
    path: 'beditmodaldetail',
    loadChildren: () => import('./beditmodaldetail/beditmodaldetail.module').then( m => m.BeditmodaldetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookcartPageRoutingModule {}
