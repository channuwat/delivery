import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CartbookeditPage } from './cartbookedit.page';

describe('CartbookeditPage', () => {
  let component: CartbookeditPage;
  let fixture: ComponentFixture<CartbookeditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartbookeditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CartbookeditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
