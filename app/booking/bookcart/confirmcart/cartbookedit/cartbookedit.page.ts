import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-cartbookedit',
  templateUrl: './cartbookedit.page.html',
  styleUrls: ['./cartbookedit.page.scss'],
})
export class CartbookeditPage implements OnInit {
  public res = '';
  public type = ''; 

  constructor(private route: ActivatedRoute, public router: Router, public navCtrl: NavController, public alertController: AlertController) { 
    this.res = route.snapshot.paramMap.get('res');
    let type = route.snapshot.paramMap.get('type');
  }

  ngOnInit() {
  }

  back() {
    // this.router.navigateByUrl("/" + this.res + "/delivery");
    this.navCtrl.navigateBack("/" + this.res + "/booking/bookcart/confirmcart");
  }

}
