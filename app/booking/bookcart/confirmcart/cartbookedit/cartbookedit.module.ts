import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CartbookeditPageRoutingModule } from './cartbookedit-routing.module';

import { CartbookeditPage } from './cartbookedit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CartbookeditPageRoutingModule
  ],
  declarations: [CartbookeditPage]
})
export class CartbookeditPageModule {}
