import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-confirmcart',
  templateUrl: './confirmcart.page.html',
  styleUrls: ['./confirmcart.page.scss'],
})
export class ConfirmcartPage implements OnInit {

  public gender;
  public moot = false;
  public moot1 = false;
  public res = '';
  public type = ''; 
  public tenantIDFileName = '';
  
  constructor(private route: ActivatedRoute, public router: Router, public navCtrl: NavController, public alertController: AlertController) {
    this.res = route.snapshot.paramMap.get('res');
    let type = route.snapshot.paramMap.get('type');
  }

  ngOnInit() {
  }
  back() {
    // this.router.navigateByUrl("/" + this.res + "/delivery");
    this.navCtrl.navigateBack("/" + this.res + "/booking/bookcart");
  }
  async checkm() {
    const alert = await this.alertController.create({
      header:'food.deltafood.co',
      message: 'ชำระเงินสำเร็จเรียบร้อบแล้วค่ะ',
      buttons: ['ตกลง']
      
    });
    await alert.present();
  }
  
  async del() {
    const alert = await this.alertController.create({
      header: 'ลบ',
      message: 'ยืนยันการลบรายการอาหารนี้ค่ะ',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'ลบ',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();
  }
  bedit() {
    // this.router.navigateByUrl('/' + this.res+"/delivery/cart");
    this.navCtrl.navigateForward('/' + this.res + "/booking/bookcart/confirmcart/cartbookedit");

  }
  openFileBrowser (event: any){
    event.preventDefault();
    let element: HTMLElement = document.getElementById('uploadfile') as HTMLElement;
    element.click();
  }
  onFileChange (event: any){
    let files = event.target.files;
    this.tenantIDFileName = files[0].name;

    console.log(files)

  }
}
