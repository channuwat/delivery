import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BeditmodaldetailPageRoutingModule } from './beditmodaldetail-routing.module';

import { BeditmodaldetailPage } from './beditmodaldetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BeditmodaldetailPageRoutingModule
  ],
  declarations: [BeditmodaldetailPage]
})
export class BeditmodaldetailPageModule {}
