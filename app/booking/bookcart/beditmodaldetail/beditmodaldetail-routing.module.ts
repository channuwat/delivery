import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BeditmodaldetailPage } from './beditmodaldetail.page';

const routes: Routes = [
  {
    path: '',
    component: BeditmodaldetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BeditmodaldetailPageRoutingModule {}
