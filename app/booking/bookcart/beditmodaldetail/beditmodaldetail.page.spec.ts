import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BeditmodaldetailPage } from './beditmodaldetail.page';

describe('BeditmodaldetailPage', () => {
  let component: BeditmodaldetailPage;
  let fixture: ComponentFixture<BeditmodaldetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeditmodaldetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BeditmodaldetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
