import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { LoginPageModule } from './login/login.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
//import { SocialLoginModule, AuthServiceConfig, FacebookLoginProvider } from 'angularx-social-login';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { WabapiService } from './wabapi.service';
import { Http, HttpModule } from '@angular/http';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ModaldetailPage } from './delivery/modaldetail/modaldetail.page';
import { ModalEditdetailPage } from './delivery/cart/modal-editdetail/modal-editdetail.page';
import { FormsModule } from '@angular/forms';
import { QmodaldetailPage } from './queue/rawqueue/queuecart/qmodaldetail/qmodaldetail.page';
import { QeditmodaldetailPage } from './queue/rawqueue/queuecart/qeditmodaldetail/qeditmodaldetail.page';
import { BmodaldetailPage } from './booking/bookcart/bmodaldetail/bmodaldetail.page';
import { BeditmodaldetailPage } from './booking/bookcart/beditmodaldetail/beditmodaldetail.page';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';

export const firebaseConfig = {
  apiKey: "AIzaSyBykAj72z0Z8xrF65xH3NPy6m7BSf70v14",
  authDomain: "panda-restaurant.firebaseapp.com",
  databaseURL: "https://panda-restaurant.firebaseio.com",
  projectId: "panda-restaurant",
  storageBucket: "panda-restaurant.appspot.com",
  messagingSenderId: "1026422127449"
};


export function HttpLoaderFactory(http: HttpClient) {
  console.log("translate");

  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}


// const config = new AuthServiceConfig([
//   {
//     id: FacebookLoginProvider.PROVIDER_ID,
//     provider: new FacebookLoginProvider('226596978081176')
//   }
// ]);
// export function provideConfig() {
//   return config;
// }


@NgModule({
  declarations: [AppComponent, ModaldetailPage, ModalEditdetailPage, QmodaldetailPage, QeditmodaldetailPage, BmodaldetailPage, BeditmodaldetailPage],
  entryComponents: [ModaldetailPage, ModalEditdetailPage, QmodaldetailPage, QeditmodaldetailPage, BmodaldetailPage, BeditmodaldetailPage],
  imports:
    [
      BrowserModule,
      IonicModule.forRoot(),
      AppRoutingModule,
      FormsModule,
      // LoginPageModule,
      //SocialLoginModule,
      HttpClientModule,
      // ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
      IonicStorageModule.forRoot(),
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }

      }),
      AngularFireModule.initializeApp(firebaseConfig),
      AngularFireDatabaseModule,
    ],

  providers: [
    StatusBar,
    WabapiService,
    SplashScreen,
    File,
    FileTransfer,

    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
